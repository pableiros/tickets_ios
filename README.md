# Tickets

Aplicaci�n m�vil para la captura de tickets.

#### Requisitos

- Cocoapods.
- Xcode 10.2.1.

#### Instalaci�n del proyecto

Ejecutamos los siguientes comandos dentro de la terminal dentro de la raiz del proyecto para instalarlo:

    $ cd Tickets
    $ pod install

A partir de aqu�, se debe de abrir el archivo `Tickets.xcworkspace` para abrir el proyecto.

