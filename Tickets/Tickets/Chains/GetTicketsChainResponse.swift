//
//  GetTicketsChainResponse.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import Alamofire

class GetTicketsChainResponse: RESTAPIChainResponse {
    
    override func handle(response: DataResponse<Any>, isHandled handled: Bool = false, completionHandler: (Bool, String?, AnyObject?) -> Void) -> Bool {
        
        if response.response?.statusCode == self.okHttpCode,
            let responseArray: [[String: AnyObject]] = response.result.value as? [[String: AnyObject]] {
            let ticketsArray: NSArray = self.createTicketsArrayFromResponse(responseArray: responseArray)
            completionHandler(true, nil, ticketsArray)
        } else {
            let ticketLocalData: TicketLocalData = TicketLocalData()
            let ticketsArray: NSArray = ticketLocalData.getAll()
            completionHandler(true, nil, ticketsArray)
        }
        
        return super.handle(response: response, isHandled: true, completionHandler: completionHandler)
    }
    
    private func createTicketsArrayFromResponse(responseArray: [[String: AnyObject]]) -> NSArray {
        let ticketsArray: NSMutableArray = NSMutableArray()
        let ticketTemplate: TicketContainer = TicketContainer()
        
        for ticketFromResponse in responseArray {
            let ticketContainer: TicketContainer = ticketTemplate.copy() as! TicketContainer
            
            if let id: Int = ticketFromResponse["id"] as? Int {
                ticketContainer.id = String(id)
            } else if let id: String = ticketFromResponse["id"] as? String {
                ticketContainer.id = id
            }
            
            ticketContainer.fechaInicio = ticketFromResponse["fecha_inicio"] as? String
            ticketContainer.fechaFin = ticketFromResponse["fecha_fin"] as? String
            
            if let folio: Int = ticketFromResponse["folio"] as? Int {
                ticketContainer.folio = String(folio)
            } else if let folio: String = ticketFromResponse["folio"] as? String {
                ticketContainer.folio = folio
            }
            
            ticketContainer.descripcion = ticketFromResponse["descripcion"] as? String
            
            if let estatus: Int = ticketFromResponse["estatus"] as? Int {
                ticketContainer.estatus = String(estatus)
            } else {
                ticketContainer.estatus = ticketFromResponse["estatus"] as? String
            }
            
            if let moduloId: Int = ticketFromResponse["modulo_id"] as? Int {
                ticketContainer.moduloId = String(moduloId)
            } else {
                ticketContainer.moduloId = ticketFromResponse["modulo_id"] as? String
            }
            
            ticketContainer.moduloNombre = ticketFromResponse["modulo_nombre"] as? String
            ticketContainer.syncId = ticketFromResponse["sync_id"] as? String
            
            ticketsArray.add(ticketContainer)
        }
        
        let ticketLocalData: TicketLocalData = TicketLocalData()
        ticketLocalData.store(tickets: ticketsArray)
        
        return ticketLocalData.getAll()
    }
}
