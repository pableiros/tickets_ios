//
//  CreateTicketChainResponse.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import Alamofire

class CreateTicketChainResponse: RESTAPIChainResponse {
    
    override func handle(response: DataResponse<Any>, isHandled handled: Bool = false, completionHandler: (Bool, String?, AnyObject?) -> Void) -> Bool {

        if response.response?.statusCode == 201,
            let responseArray: [String: AnyObject] = response.result.value as? [String: AnyObject] {
            
            let ticketContainer: TicketContainer = TicketContainer()
            ticketContainer.id = String(responseArray["id"] as! Int)
            ticketContainer.folio = String(responseArray["folio"] as! Int)
            
            completionHandler(true, nil, ticketContainer)
        } else {
            completionHandler(false, nil, nil)
        }
        
        return super.handle(response: response, isHandled: true, completionHandler: completionHandler)
    }
}
