//
//  TicketChain.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class TicketChain {
    
    var ticketContainer: TicketContainer?
    var nextChain: TicketChain?
    
    func handle(completionHandler: @escaping (Bool, String?, AnyObject?) -> Void) {
        let ticketModel: TicketModel = TicketModel()
        ticketModel.create(ticketContainer: self.ticketContainer!) { (success, message, data) in
            if success == false {
                self.nextChain = nil
            }
            
            self.dispatchNextChain(completionHandler: completionHandler)
        }
    }
    
    private func dispatchNextChain(completionHandler: @escaping (Bool, String?, AnyObject?) -> Void) {
        if self.nextChain != nil {
            self.nextChain?.handle(completionHandler: completionHandler)
        } else {
            completionHandler(true, nil, nil)
        }
    }
    
    static func createChain(tickets: NSArray) -> TicketChain? {
        let ticketsReversed = tickets.reversed()
        var ticketNextChain: TicketChain?
        
        for ticketObj in ticketsReversed {
            let existingLink: TicketChain? = ticketNextChain
            
            let ticketChain: TicketChain = TicketChain()
            ticketChain.ticketContainer = ticketObj as? TicketContainer
            
            ticketNextChain = ticketChain
            ticketNextChain?.nextChain = existingLink
        }
        
        return ticketNextChain
    }
}
