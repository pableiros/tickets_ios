//
//  GetModulosChainResponse.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import Alamofire

class GetModulosChainResponse: RESTAPIChainResponse {
    
    override func handle(response: DataResponse<Any>, isHandled handled: Bool = false, completionHandler: (Bool, String?, AnyObject?) -> Void) -> Bool {
        
        if response.response?.statusCode == self.okHttpCode,
            let responseArray: [[String: AnyObject]] = response.result.value as? [[String: AnyObject]] {
            let modulosArray: NSArray = self.createModulosArrayFromResponse(responseArray: responseArray)
            completionHandler(true, nil, modulosArray)
        } else {
            let moduloLocalData: ModuloLocalData = ModuloLocalData()
            let modulosArray: NSArray = moduloLocalData.getAll()
            completionHandler(true, nil, modulosArray)
        }
        
        return super.handle(response: response, isHandled: true, completionHandler: completionHandler)
    }
    
    private func createModulosArrayFromResponse(responseArray: [[String: AnyObject]]) -> NSArray {
        let modulosArray: NSMutableArray = NSMutableArray()
        let moduloTemplate: ModuloContainer = ModuloContainer()
        
        for moduloFromResponse in responseArray {
            let moduloContainer: ModuloContainer = moduloTemplate.copy() as! ModuloContainer
            
            if let id: Int = moduloFromResponse["id"] as? Int {
                moduloContainer.id = String(id)
            } else if let id: String = moduloFromResponse["id"] as? String {
                moduloContainer.id = id
            }
            
            moduloContainer.nombre = moduloFromResponse["nombre"] as? String
            
            modulosArray.add(moduloContainer)
        }
        
        let moduloLocalData: ModuloLocalData = ModuloLocalData()
        moduloLocalData.store(modulos: modulosArray)
    
        return modulosArray
    }
}
