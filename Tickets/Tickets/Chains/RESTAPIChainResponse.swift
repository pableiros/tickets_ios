//
//  RESTAPIChainResponse.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import Alamofire

class RESTAPIChainResponse {
    
    var nextResponse: RESTAPIChainResponse?
    var afterCompletion: (() -> Void)?
    var anyObjectPassed: AnyObject?
    var statusCode: Int?
    
    let okHttpCode = 200
    let badRequest = 400
    let forbidden = 403
    let notFoundRequest = 404
    let conflictResponse = 409
    let preFailedResponse = 412
        
    required init() { }
    
    func handle(response: DataResponse<Any>, isHandled handled: Bool = false, completionHandler: (Bool, String?, AnyObject?) -> Void) -> Bool {
        if nextResponse != nil {
            return (nextResponse?.handle(response: response, isHandled: handled, completionHandler: completionHandler))!
        } else if !handled {
            completionHandler(false, "", nil)
        }
        
        return handled
    }
    
    func breakForLinkFound() {
        nextResponse = nil
    }
    
    class func create(chain chainResponseClases: [RESTAPIChainResponse.Type], with anyObjectPassed: AnyObject?) -> RESTAPIChainResponse? {
        var restApiChainResponse: RESTAPIChainResponse?
        
        for typeClass in chainResponseClases.reversed() {
            let existingLink = restApiChainResponse
            restApiChainResponse = typeClass.init()
            restApiChainResponse?.nextResponse = existingLink
            restApiChainResponse?.anyObjectPassed = anyObjectPassed
        }
        
        return restApiChainResponse
    }
    
    func getStringValueConvertedToIntValue(fromDictionary responseDictionary: [String: AnyObject], forKey key: String) -> Int {
        var value: Int = 0
        
        if let dictValue = responseDictionary[key] as? Int {
            value = dictValue
        } else if let dictValue = responseDictionary[key] as? String {
            value = Int(dictValue)!
        }
        
        return value
    }
}
