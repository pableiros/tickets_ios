//
//  ConfigController.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class ConfigController {
    
    private let documentName: String = "Config"
    private let documentExtension: String = "plist"
    
    func object(forKey key: String) -> NSDictionary {
        let result: NSDictionary!
        
        if let cachedVersion: NSDictionary = Global.shared.configCache.object(forKey: key as NSString) {
            result = cachedVersion
        } else {
            let pListUrl: URL? = Bundle.main.url(forResource: self.documentName, withExtension: self.documentExtension)
            let plistDocument: NSDictionary = NSDictionary(contentsOf: pListUrl!)! as NSDictionary
            result = plistDocument.object(forKey: key) as? NSDictionary
            Global.shared.configCache.setObject(result, forKey: key as NSString)
        }
        
        return result
    }
}
