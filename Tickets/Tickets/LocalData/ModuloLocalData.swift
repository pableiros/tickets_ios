//
//  ModuloLocalData.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import RealmSwift

class ModuloLocalData {
    
    func store(modulos: NSArray) {
        let realm: Realm! = try! Realm()
        realm.beginWrite()
        
        for moduloObj in modulos {
            let moduloContainer: ModuloContainer = moduloObj as! ModuloContainer
            let moduloRealm: ModuloRealm = moduloContainer.toModuloRealm()
            realm.add(moduloRealm)
        }
        
        try! realm.commitWrite()
    }
    
    func getAll() -> NSArray {
        let realm: Realm! = try! Realm()
        let modulosRealm: Results<ModuloRealm> = realm.objects(ModuloRealm.self)
        let modulosArray: NSMutableArray = NSMutableArray()
        
        let modulos: [ModuloContainer] = modulosRealm.map { $0.toModuloContainer() }
        modulosArray.addObjects(from: modulos)
        
        if modulosArray.count == 0 {
            modulosArray.add(ModuloContainer.createComprasInstance())
            modulosArray.add(ModuloContainer.createGastosViajeInstance())
            modulosArray.add(ModuloContainer.createGastosMantenimientoInstance())
            modulosArray.add(ModuloContainer.createProcesosInstance())
            modulosArray.add(ModuloContainer.createRecursoshumanosInstance())
        }
        
        return modulosArray
    }
}
