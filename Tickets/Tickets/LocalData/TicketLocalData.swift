//
//  TicketLocalData.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import RealmSwift

class TicketLocalData {
    
    func store(tickets: NSArray) {
        let realm: Realm! = try! Realm()
        realm.beginWrite()
        
        let ticketsRealm: Results<TicketRealm> = realm.objects(TicketRealm.self).filter("folio != ''")
        realm.delete(ticketsRealm)

        for ticketObj in tickets {
            let ticketContainer: TicketContainer = ticketObj as! TicketContainer
            
            if realm.objects(TicketRealm.self).filter("syncId != '\(ticketContainer.syncId ?? "")'").first == nil {
                let ticketRealm: TicketRealm = ticketContainer.toTicketRealm()
                realm.add(ticketRealm)
            }
        }
        
        try! realm.commitWrite()
    }

    func getAll() -> NSArray {
        let realm: Realm! = try! Realm()
        let ticketsRealm: Results<TicketRealm> = realm.objects(TicketRealm.self)
        let ticketsArray: NSMutableArray = NSMutableArray()
        
        let tickets: [TicketContainer] = ticketsRealm.map { $0.toTicketContainer() }
        ticketsArray.addObjects(from: tickets)
        return ticketsArray
    }
    
    func create(ticketContainer: TicketContainer) {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        ticketContainer.fechaInicio = dateFormatter.string(from: Date())
        ticketContainer.syncId = UUID().uuidString
        
        let realm: Realm! = try! Realm()
        realm.beginWrite()
        realm.add(ticketContainer.toTicketRealm())
        try! realm.commitWrite()
    }
    
    func update(ticketContainer: TicketContainer, newTicketContainer: TicketContainer) {
        let realm: Realm! = try! Realm()
        let ticketRealm: TicketRealm? = realm.objects(TicketRealm.self).filter { $0.syncId == ticketContainer.syncId }.first
    
        realm.beginWrite()

        ticketRealm?.folio = newTicketContainer.folio ?? ""
        ticketRealm?.id = newTicketContainer.id ?? ""
        
        try! realm.commitWrite()
    }

    func getPendingToSync() -> NSArray {
        let realm: Realm! = try! Realm()
        let ticketsRealm: Results<TicketRealm> = realm.objects(TicketRealm.self).filter("folio == ''")
        let ticketsArray: NSMutableArray = NSMutableArray()
        let tickets: [TicketContainer] = ticketsRealm.map { $0.toTicketContainer() }
        ticketsArray.addObjects(from: tickets)
        return ticketsArray
    }
    
    func getTicket(ticketContainer: TicketContainer) -> TicketContainer {
        let realm: Realm! = try! Realm()
        let ticketRealm: TicketRealm? = realm.objects(TicketRealm.self).filter { $0.syncId == ticketContainer.syncId }.first
        var ticketToReturn: TicketContainer = ticketContainer
        
        if ticketRealm != nil {
            ticketToReturn = ticketRealm!.toTicketContainer()
        }
        
        return ticketToReturn
    }
}
