//
//  ValidatorResultContainer.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class ValidatorResultContainer {
    var success: Bool = false
    var title: String?
    var message: String?
    var validatorContainer: AnyObject?
}
