//
//  TicketFormValidator.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class TicketFormValidator {
    
    func validate(ticketContainer: TicketContainer) -> ValidatorResultContainer {
        var title: String?
        var messageResult: String?
        let ticketFormValidatorContainer: TicketFormValidatorContainer = TicketFormValidatorContainer()
        
        ticketFormValidatorContainer.isModuloValid = ticketContainer.moduloContainer != nil
        
        if ticketFormValidatorContainer.isModuloValid == false {
            title = Localizables.ticketFormTableViewControllerNoModuloSelected.translate()
            messageResult = Localizables.ticketFormTableViewControllerNoModuloSelectedMessage.translate()
        }
        
        ticketFormValidatorContainer.isDescripcionValid = ticketContainer.descripcion?.isFullEmpty() == false
        
        if ticketFormValidatorContainer.isDescripcionValid == false, title == nil {
            title = Localizables.ticketFormTableViewControllerNoDescripcion.translate()
            messageResult = Localizables.ticketFormTableViewControllerNoDescripcionMessage.translate()
        }
    
        let validatorResultContainer: ValidatorResultContainer = ValidatorResultContainer()
        validatorResultContainer.success = messageResult == nil
        validatorResultContainer.title = title
        validatorResultContainer.message = messageResult
        validatorResultContainer.validatorContainer = ticketFormValidatorContainer
        return validatorResultContainer
    }
}
