//
//  ModuloFacade.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class ModuloFacade: ListBaseFacade {
    
    override init() {
        super.init()
        self.model = ModuloModel()
        self.cache = Global.shared.modulosCache
    }
}
