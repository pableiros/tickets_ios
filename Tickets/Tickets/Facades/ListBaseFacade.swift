//
//  ListBaseFacade.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class ListBaseFacade: ListFacadeDataSource {
    
    var cache: NSCache<NSString, NSArray>?
    var model: ListModelDataSource!
    
    var cachedArrayKey: NSString {
        get {
            return "array"
        }
    }
    
    func isValidForCache(key: NSString, completionHandler: @escaping  (Bool, String?, AnyObject?) -> Void) -> Bool {
        var isValidForCache: Bool = false
        
        if let cachedArray: NSArray = self.cache?.object(forKey: key) {
            if cachedArray.count > 0 {
                let result: NSArray = self.updateResultsArray(array: cachedArray)
                completionHandler(true, nil, result)
                isValidForCache = true
            }
        }
        
        return isValidForCache
    }
    
    func updateResultsArray(array: NSArray) -> NSArray {
        return array
    }
    
    func completionHandlerForGetResult(cacheKey: NSString, completionHandler: @escaping (Bool, String?, AnyObject?) -> Void) -> ((Bool, String?, AnyObject?) -> Void) {
        return { success, message, data in // <-- don't prevent unowned self
            var result: AnyObject? = nil
            
            if success {
                if let dataArray: NSArray = data as? NSArray {
                    self.setArrayToCache(array: dataArray, cacheKey: cacheKey)
                    result = self.updateResultsArray(array: dataArray)
                }
            } else {
                result = data
            }
            
            completionHandler(success, message, result)
        }
    }
    
    func setArrayToCache(array: NSArray, cacheKey: NSString? = nil) {
        var key: NSString = self.cachedArrayKey
        
        if cacheKey != nil {
            key = cacheKey!
        }
        
        self.cache?.setObject(array, forKey: key)
    }
    
    // MARK - list facade protocol
    
    func get(completionHandler: @escaping (Bool, String?, AnyObject?) -> Void) {
        guard self.isValidForCache(key: self.cachedArrayKey, completionHandler: completionHandler) == false else {
            return
        }
        
        self.model.get(completionHandler: self.completionHandlerForGetResult(cacheKey: self.cachedArrayKey, completionHandler: completionHandler))
    }
    
    func requireFetchManually() -> Bool {
        guard let cachedArray: NSArray = self.cache?.object(forKey: self.cachedArrayKey) else { return true }
        return cachedArray.count == 0
    }
    
    func prepareGetManually() {
        self.cache?.removeAllObjects()
    }
    
    func addContainerToList(baseContainer: ListBaseContainer) {
        guard let listCached: NSArray = self.cache?.object(forKey: self.cachedArrayKey) else { return }
        
        let listMutableCached: NSMutableArray = NSMutableArray(array: listCached)
        listMutableCached.insert(baseContainer, at: 0)
        
        self.setArrayToCache(array: listMutableCached)
    }
    
    func updateContainerOnList(baseContainer: ListBaseContainer) {
        guard let listCached: NSArray = self.cache?.object(forKey: self.cachedArrayKey) else { return }
        
        let listMutableCached: NSMutableArray = NSMutableArray(array: listCached)
        let indexToReplace: Int = listMutableCached.index(of: baseContainer)
        listMutableCached.replaceObject(at: indexToReplace, with: baseContainer)
        
        self.setArrayToCache(array: listMutableCached)
    }
    
    func deleteCachedObject(containerToDelete: ListBaseContainer) {
        guard let cachedArray: NSArray = self.cache?.object(forKey: self.cachedArrayKey) else { return }
        
        let cachedMutableArray: NSMutableArray = NSMutableArray(array: cachedArray)
        var indexToDelete: Int?
        
        cachedLoop: for index in 0..<cachedMutableArray.count {
            let objectToCompare: ListBaseContainer? = cachedMutableArray[index] as? ListBaseContainer
            
            if objectToCompare?.id == containerToDelete.id {
                indexToDelete = index
                break cachedLoop
            }
        }
        
        if indexToDelete != nil {
            cachedMutableArray.removeObject(at: indexToDelete!)
            self.cache?.setObject(cachedMutableArray, forKey: self.cachedArrayKey)
        }
    }
}
