//
//  TicketFacade.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class TicketFacade: ListBaseFacade {
    
    private var _operationQueue: OperationQueue!
    private var operationQueue: OperationQueue {
        get {
            if self._operationQueue == nil {
                self._operationQueue = OperationQueue()
                self._operationQueue.name = "Create tickets in background operation queue"
                self._operationQueue.maxConcurrentOperationCount = 1
            }
            
            return self._operationQueue
        }
    }
    
    override init() {
        super.init()
        self.model = TicketModel()
        self.cache = Global.shared.ticketsCache
    }

    func create(ticketContainer: TicketContainer, completionHandler: @escaping (Bool, String?, AnyObject?) -> Void) {
        let ticketLocalData: TicketLocalData = TicketLocalData()
        ticketLocalData.create(ticketContainer: ticketContainer)

        let ticketModel: TicketModel = TicketModel()
        ticketModel.create(ticketContainer: ticketContainer, completionHandler: completionHandler)
    }
    
    func performBackgroundCreation() {
        self.operationQueue.cancelAllOperations()
        self.operationQueue.addOperation {
            let ticketLocalData: TicketLocalData = TicketLocalData()
            let ticketsPendingToSync: NSArray = ticketLocalData.getPendingToSync()
            
            let ticketChain: TicketChain? = TicketChain.createChain(tickets: ticketsPendingToSync)
            ticketChain?.handle(completionHandler: { (_, _, _) in
            })
        }
    }
}
