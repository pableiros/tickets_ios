//
//  EndPointModel.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class EndPointModel {
    
    var baseRESTAPIModel: BaseRESTAPIModel!
    
    var endPointSelected: String {
        fatalError("Not implemented")
    }
    
    let endPointSelectedKey: String = "End Points Selected"
    let endPointsKey: String = "End Points"
    let apiKeysKey: String = "ApiKeys"
    let apiKeyKey: String = "api_key"
    let apiSecretKey: String = "api_secret"
    let domainsKey: String = "Domains"
    let domainKey: String = "Domain"
    let uriKey: String = "URI"
    let apiUriKey: String = "ApiUri"
    
    private func getEndPointStringKey(forKey key: String) -> String {
        return String(format: "EndPoint-%@", key)
    }
    
    func getEndPointSelected() -> String {
        return self.endPointSelected
    }
    
    func apiKeyApiSecretDictionary() -> [String: String] {
        let domainSelected: NSDictionary = self.getDomainSelected()
        
        guard let apiKeySecretConfigDictionary: NSDictionary = domainSelected[self.apiKeysKey] as? NSDictionary else {
            fatalError("api keys not found")
        }
        
        var apiKeySecretDictionary: [String: String] = [String: String]()
        apiKeySecretDictionary["api_key"] = (apiKeySecretConfigDictionary[self.apiKeyKey] as? String)!
        apiKeySecretDictionary["api_secret"] = (apiKeySecretConfigDictionary[self.apiSecretKey] as? String)!
        return apiKeySecretDictionary
    }
    
    func getRESTApiModel() -> BaseRESTAPIModel {
        if self.baseRESTAPIModel == nil {
            self.baseRESTAPIModel = BaseRESTAPIModel()
        }
        
        return self.baseRESTAPIModel
    }
    
    func url(forKey key: String) -> String {
        let endPoint: NSDictionary = self.getEndPoint()
        let domainSelected: NSDictionary = self.getDomainSelected()
        
        guard let domainOpt: String = domainSelected[self.domainKey] as? String  else {
            fatalError("domains not found")
        }
        
        guard let apiUriOpt: String = domainSelected[self.apiUriKey] as? String else {
            fatalError("uri not found")
        }
        guard let urisDictionary: NSDictionary = endPoint[self.uriKey] as? NSDictionary else {
            fatalError("uri not found")
        }
        
        guard let uriOpt: String = urisDictionary[key] as? String else {
            fatalError("uri not found")
        }
        
        let domain: String = domainOpt
        let apiUri: String = apiUriOpt
        let baseUrl = String(format: "%@%@", domain, apiUri)
        let uri: String = uriOpt
        let url: String = String(format: "%@%@", baseUrl, uri)
        
        return url
    }
    
    func getDomainSelected() -> NSDictionary {
        let endPoint: NSDictionary = self.getEndPoint()
        let endPointSelectedKey: String = self.getEndPointSelected()
        
        let endPointSelected: String = self.getEndPointSelected(forKey: endPointSelectedKey)
        
        guard let domains: NSDictionary = endPoint[self.domainsKey] as? NSDictionary,
            let domainSelected: NSDictionary = domains[endPointSelected] as? NSDictionary else {
                fatalError("domains not found")
        }
        
        return domainSelected
    }
    
    func getEndPoint() -> NSDictionary {
        return self.getEndPoint(forKey: self.getEndPointSelected())
    }
    
    func getEndPoint(forKey key: String) -> NSDictionary {
        let endPointsDictionary: NSDictionary = self.valueFromConfigFile(forKey: self.endPointsKey)
        
        guard let endPoint: NSDictionary = endPointsDictionary[key] as? NSDictionary else {
            fatalError("End point not found")
        }
        
        return endPoint
    }
    
    func getEndPointSelected(forKey key: String) -> String {
        let endPointsDictionary: NSDictionary = self.valueFromConfigFile(forKey: self.endPointSelectedKey)
        
        guard let endPoint: String = endPointsDictionary[key] as? String else {
            fatalError("End point not found")
        }
        
        let endPointKey: String = self.getEndPointStringKey(forKey: key)
        if let endPointValue: String = self.getEndPointSelectedFromInternalStorage(endPointKey: endPointKey) {
            return endPointValue
        }
        
        let endPointValue: String = ProcessInfoHelper.getValue(fromKey: endPointKey, withDefault: endPoint)
        return endPointValue
    }
    
    func valueFromConfigFile(forKey key: String) -> NSDictionary {
        let configController: ConfigController = ConfigController()
        return configController.object(forKey: key) as NSDictionary
    }
    
    // MARK: - internal end point selected for debug
    
    func changeEndPointSelected(newEndPointKey: String, forKey key: String) {
        let endPointKey: String = self.getEndPointStringKey(forKey: key)
        UserDefaults.standard.setValue(newEndPointKey, forKey: endPointKey)
    }
    
    private func getEndPointSelectedFromInternalStorage(endPointKey: String) -> String? {
        return UserDefaults.standard.value(forKey: endPointKey) as? String
    }
}
