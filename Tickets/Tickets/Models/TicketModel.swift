//
//  TicketModel.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import Alamofire

class TicketModel: BaseTicketsModel, ListModelDataSource {
    
    func get(completionHandler: @escaping (Bool, String?, AnyObject?) -> Void) {
        let url: String = self.url(forKey: "Get Tickets")
        let restApiModel: BaseRESTAPIModel = self.getRESTApiModel()
        restApiModel.get(url,
                         parameters: nil,
                         headers: nil,
                         chainResponse: [GetTicketsChainResponse.self],
                         completionHandler: completionHandler)
    }

    func create(ticketContainer: TicketContainer, completionHandler: @escaping (Bool, String?, AnyObject?) -> Void) {
        let url :String = self.url(forKey: "Create Ticket")
        let restApiModel: BaseRESTAPIModel = self.getRESTApiModel()
        
        let postCompletionHandler: (Bool, String?, AnyObject?) -> Void = { success, message, data in
            if success {
                let ticketLocalData: TicketLocalData = TicketLocalData()
                ticketLocalData.update(ticketContainer: ticketContainer, newTicketContainer: data as! TicketContainer)
            }
            
            completionHandler(success, message, data)
        }
        
        restApiModel.post(url,
                          parameters: self.prepareCreateParams(ticketContainer: ticketContainer),
                          headers: nil,
                          encoding: JSONEncoding.default,
                          chainResponse: [CreateTicketChainResponse.self],
                          completionHandler: postCompletionHandler)
    }
    
    private func prepareCreateParams(ticketContainer: TicketContainer) -> [String: String] {
        var params: [String: String] = [String: String]()
        params["fecha_inicio"] = ticketContainer.fechaInicio
        params["descripcion"] = ticketContainer.descripcion
        params["modulo_id"] = ticketContainer.moduloId
        params["sync_id"] = ticketContainer.syncId
        return params
    }
}
