//
//  ModuloModel.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class ModuloModel: BaseTicketsModel, ListModelDataSource {
    
    func get(completionHandler: @escaping (Bool, String?, AnyObject?) -> Void) {
        let url: String = self.url(forKey: "Get Modulos")
        let restApiModel: BaseRESTAPIModel = self.getRESTApiModel()
        restApiModel.get(url,
                         parameters: nil,
                         headers: nil,
                         chainResponse: [GetModulosChainResponse.self],
                         completionHandler: completionHandler)
    }
}
