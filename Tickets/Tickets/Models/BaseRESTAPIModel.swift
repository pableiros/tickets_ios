//
//  BaseRESTAPIModel.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import Alamofire

class BaseRESTAPIModel {
    
    var addInternalServerChainError: Bool = true
    var afterCompletion: (() -> Void)?
    var anyObjectPassed: AnyObject?
    var addManuallyCancellation: Bool = false
    var printRawResponse: Bool = false
    
    private var session: Request!
    
    private var suspendNotification: NSObjectProtocol!
    private var resumeNotification: NSObjectProtocol!
    private var cancelNotification: NSObjectProtocol!
    
    private let authorizationHeader: String = "Authorization"
    private let authorizationValuePrefix: String = "Bearer "
    
    private struct APIManager {
        static let sharedManager: SessionManager = {
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 300
            return SessionManager(configuration: configuration)
        }()
    }
    
    func apiKeySecretDictionaryForRESTAPI() -> [String: String] {
        return [String: String]()
    }
    
    func tokenForRequest() -> String! {
        assertionFailure("Not implemented")
        return nil
    }
    
    // MARK: - requests
    
    func post(_ url: String,
              urlParameters: [String: String]? = nil,
              parameters: [String: Any]?,
              headers: [String: String]?,
              encoding: ParameterEncoding,
              chainResponse: [RESTAPIChainResponse.Type],
              completionHandler: @escaping CompletionHandler) {
        var apiKeySecretDictionary: [String: String] = self.apiKeySecretDictionaryForRESTAPI()
        if urlParameters != nil {
            apiKeySecretDictionary.update(urlParameters!)
        }
        
        let urlToRequest: URL? = URL(string: url)?.appendQuery(parameters: apiKeySecretDictionary)
        
        let finalHeaders: [String: String]? = self.createHeaderForRequest(headers)
        
        #if DEBUG
        Logger.shared.debug("-------------------------------------------------------- REQUEST --------------------------------------------------------")
        Logger.shared.debug("POST: \(url)")
        if parameters != nil {
            Logger.shared.debug("Parameters: \(parameters!.toJSONText())")
        }
        
        if let headers: [String: String] = finalHeaders, headers.count > 0 {
            Logger.shared.debug(String(format: "HEADERS: %@", headers.toJSONText()))
        }
        #endif
        
        self.startNetworkActivityIndicator()
        
        if let request: DataRequest = self.performRequest((urlToRequest?.absoluteString)!,
                                                          method: .post,
                                                          parameters: parameters,
                                                          encoding: encoding,
                                                          headers: finalHeaders,
                                                          finalChainResponse: chainResponse,
                                                          completionHandler: completionHandler) {
            self.handleInterruptions(request)
        }
    }
    
    func get(_ url: String,
             parameters: [String: String]?,
             headers: [String: String]?,
             chainResponse: [RESTAPIChainResponse.Type],
             completionHandler: @escaping CompletionHandler) {
        self.startNetworkActivityIndicator()
        
        var apiKeySecretDictionary: [String: String] = self.apiKeySecretDictionaryForRESTAPI()
        
        if parameters != nil {
            apiKeySecretDictionary.update(parameters!)
        }
        
        let finalHeaders: [String: String]? = self.createHeaderForRequest(headers)
        
        #if DEBUG
        Logger.shared.debug("-------------------------------------------------------- REQUEST --------------------------------------------------------")
        Logger.shared.debug(String(format: "GET: %@", url))
        
        if let headers: [String: String] = finalHeaders, headers.count > 0 {
            Logger.shared.debug(String(format: "HEADERS: %@", headers.toJSONText()))
        }
        #endif
        
        if let request: DataRequest = self.performRequest(url,
                                                          method: .get,
                                                          parameters: apiKeySecretDictionary,
                                                          encoding: URLEncoding.queryString,
                                                          headers: finalHeaders,
                                                          finalChainResponse: chainResponse,
                                                          completionHandler: completionHandler) {
            
            if self.addManuallyCancellation {
                self.session = request
            }
            
            self.handleInterruptions(request)
        }
    }
    
    func put(_ url: String,
             urlParameters: [String: String]? = nil,
             parameters: [String: AnyObject]?,
             headers: [String: String]?,
             encoding: ParameterEncoding = URLEncoding.httpBody,
             chainResponse: [RESTAPIChainResponse.Type],
             completionHandler: @escaping CompletionHandler) {
        
        self.startNetworkActivityIndicator()
        
        let finalHeaders: [String: String]? = self.createHeaderForRequest(headers)
        
        var apiKeySecretDictionary: [String: String] = self.apiKeySecretDictionaryForRESTAPI()
        if urlParameters != nil {
            apiKeySecretDictionary.update(urlParameters!)
        }
        
        let urlToRequest = URL(string: url)?.appendQuery(parameters: apiKeySecretDictionary)
        
        #if DEBUG
        Logger.shared.debug("-------------------------------------------------------- REQUEST --------------------------------------------------------")
        Logger.shared.debug(String(format: "PUT: %@", url))
        
        if parameters != nil {
            Logger.shared.debug("Parameters: \(parameters!.toJSONText())")
        }
        #endif
        
        if let request: DataRequest = self.performRequest(urlToRequest!,
                                                          method: .put,
                                                          parameters: parameters,
                                                          encoding: encoding,
                                                          headers: finalHeaders,
                                                          finalChainResponse: chainResponse,
                                                          completionHandler: completionHandler) {
            self.handleInterruptions(request)
        }
    }
    
    func delete(_ url: String,
                parameters: [String: String]?,
                headers: [String: String]?,
                chainResponse: [RESTAPIChainResponse.Type],
                completionHandler: @escaping CompletionHandler) {
        
        self.startNetworkActivityIndicator()
        
        let finalHeaders: [String: String]? = self.createHeaderForRequest(headers)
        
        #if DEBUG
        Logger.shared.debug("-------------------------------------------------------- REQUEST --------------------------------------------------------")
        Logger.shared.debug(String(format: "DELETE: %@", url))
        #endif
        
        var apiKeySecretDictionary: [String: String] = self.apiKeySecretDictionaryForRESTAPI()
        if parameters != nil {
            apiKeySecretDictionary.update(parameters!)
        }
        
        if let request: DataRequest = self.performRequest(url,
                                                          method: .delete,
                                                          parameters: apiKeySecretDictionary,
                                                          encoding: URLEncoding.queryString,
                                                          headers: finalHeaders,
                                                          finalChainResponse: chainResponse,
                                                          completionHandler: completionHandler) {
            self.handleInterruptions(request)
        }
    }
    
    func performRequest(_ url: URLConvertible,
                        method: HTTPMethod,
                        parameters: [String: Any]?,
                        encoding: ParameterEncoding = URLEncoding.default,
                        headers: HTTPHeaders? = nil,
                        finalChainResponse: [RESTAPIChainResponse.Type],
                        completionHandler: @escaping CompletionHandler) -> DataRequest? {
        let request = APIManager.sharedManager.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
        return request.responseJSON  { [unowned self] dataResponse in
            self.logResponse(dataResponse)
            self.responseJSONHandler(dataResponse, requestName: method.rawValue, finalChainResponse: finalChainResponse, completionHandler: completionHandler)
        }
    }
    
    func responseJSONHandler(_ response: DataResponse<Any>, requestName: String, finalChainResponse: [RESTAPIChainResponse.Type], completionHandler: CompletionHandler) {
        guard (response.error as NSError?)?.code != NSURLErrorCancelled else {
            return
        }
        
        #if DEBUG
        Logger.shared.debug("\(requestName) Request completed")
        Logger.shared.debug("Rest response: \(response)")
        
        if self.printRawResponse, let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
            Logger.shared.debug("Raw response: \(utf8Text)")
        }
        
        if response.response != nil {
            Logger.shared.debug("Http code: \((response.response?.statusCode)!)")
        }
        #endif
        
        self.prepareFinishedRequest()
        
        if let chain: RESTAPIChainResponse = RESTAPIChainResponse.create(chain: finalChainResponse, with: self.anyObjectPassed) {
            chain.afterCompletion = self.afterCompletion
            chain.anyObjectPassed = self.anyObjectPassed
            
            let handled: Bool = chain.handle(response: response, completionHandler: completionHandler)
            
            #if DEBUG
            Logger.shared.debug("\(requestName) request handled: \(handled)")
            #endif
        }
    }
    
    // MARK: - request preparations
    
    func createHeaderForRequest(_ userHeaders: [String: String]?) -> [String: String]? {
        var headersForRequest = [String: String]()
        
        if let _ = userHeaders {
            headersForRequest.update(userHeaders!)
        }
        
        return headersForRequest
    }
    
    
    // MARK: - handle interruptions
    
    func handleInterruptions(_ request: Request) {
        self.onSuspendSessionDataTask(request)
        self.onResumeSessionDataTask(request)
        self.onCancelSessionDataTask(request)
    }
    
    private func onSuspendSessionDataTask(_ session: Request) {
        self.suspendNotification = NotificationHelper.addNotificationForNameOnMainQueue(GlobalNotificationKeys.suspendApplication) { _ in
            self.stopNetworkActivityIndicator()
            session.suspend()
        }
    }
    
    private func onResumeSessionDataTask(_ session: Request) {
        self.resumeNotification = NotificationHelper.addNotificationForNameOnMainQueue(GlobalNotificationKeys.resumeApplication) { _ in
            self.startNetworkActivityIndicator()
            session.resume()
        }
    }
    
    private func onCancelSessionDataTask(_ session: Request) {
        self.cancelNotification = NotificationHelper.addNotificationForNameOnMainQueue(GlobalNotificationKeys.cancelApplication) { _ in
            self.stopNetworkActivityIndicator()
            session.cancel()
        }
    }
    
    private func removeInternetObservers() {
        if self.suspendNotification != nil {
            NotificationHelper.removeObserverNotification(self.suspendNotification)
        }
        if self.resumeNotification != nil {
            NotificationHelper.removeObserverNotification(self.resumeNotification)
        }
        
        if self.cancelNotification != nil {
            NotificationHelper.removeObserverNotification(self.cancelNotification)
        }
        
        self.suspendNotification = nil
        self.resumeNotification = nil
        self.cancelNotification = nil
    }
    
    func cancelRequest() {
        self.stopNetworkActivityIndicator()
        
        if self.session != nil {
            self.session.cancel()
        }
    }
    
    // MARK: - network activity indicator
    
    func startNetworkActivityIndicator() {
        OperationQueue.main.addOperation {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
    
    func stopNetworkActivityIndicator() {
        OperationQueue.main.addOperation {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    // MARK: - handlers
    
    func prepareFinishedRequest() {
        self.stopNetworkActivityIndicator()
        self.removeInternetObservers()
    }
    
    fileprivate func logResponse(_ response: DataResponse<Any>) {
        #if DEBUG
        Logger.shared.debug("-------------------------------------------------------- RESPONSE --------------------------------------------------------")
        if let response = response.response {
            Logger.shared.debug("response: \(response.url!)")
        } else {
            Logger.shared.warning("response is nil")
        }
        #endif
    }
}
