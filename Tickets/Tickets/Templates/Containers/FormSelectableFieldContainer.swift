//
//  FormSelectableFieldContainer.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class FormSelectableFieldContainer {
    var title: String!
    var isValid: Bool!
    var descriptionContent: String?
    var descriptionForNotSelected: String!
    var isEnabled: Bool = true
}
