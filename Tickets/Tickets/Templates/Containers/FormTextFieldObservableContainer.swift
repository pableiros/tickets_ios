//
//  FormTextFieldObservableContainer.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class FormTextFieldObservableContainer {
    
    var setTextToModel: ((String) -> Void)!
}
