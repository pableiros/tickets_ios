//
//  GlobalNotificationKeys.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

struct GlobalNotificationKeys {
    
    static let suspendApplication = "Suspend Application Notification Key"
    static let resumeApplication = "Resume Application Notification Key"
    static let cancelApplication = "Cancel Application Notification Key"
}
