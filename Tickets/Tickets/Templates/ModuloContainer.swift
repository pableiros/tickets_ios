//
//  ModuloContainer.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class ModuloContainer: ListBaseContainer, NSCopying {

    var nombre: String?

    override func getCellValue() -> String {
        return nombre ?? ""
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return ModuloContainer()
    }
    
    func toModuloRealm() -> ModuloRealm {
        let moduloRealm: ModuloRealm = ModuloRealm()
        moduloRealm.id = self.id
        moduloRealm.nombre = self.nombre
        return moduloRealm
    }
    
    static func createComprasInstance() -> ModuloContainer {
        let moduloContainer: ModuloContainer = ModuloContainer()
        moduloContainer.id = "1"
        moduloContainer.nombre = "Compras"
        return moduloContainer
    }
    
    static func createGastosViajeInstance() -> ModuloContainer {
        let moduloContainer: ModuloContainer = ModuloContainer()
        moduloContainer.id = "2"
        moduloContainer.nombre = "Gastos de viaje"
        return moduloContainer
    }
    
    static func createGastosMantenimientoInstance() -> ModuloContainer {
        let moduloContainer: ModuloContainer = ModuloContainer()
        moduloContainer.id = "3"
        moduloContainer.nombre = "Mantenimiento"
        return moduloContainer
    }
    
    static func createProcesosInstance() -> ModuloContainer {
        let moduloContainer: ModuloContainer = ModuloContainer()
        moduloContainer.id = "4"
        moduloContainer.nombre = "Procesos"
        return moduloContainer
    }
    
    static func createRecursoshumanosInstance() -> ModuloContainer {
        let moduloContainer: ModuloContainer = ModuloContainer()
        moduloContainer.id = "5"
        moduloContainer.nombre = "Recursos humanos"
        return moduloContainer
    }
}
