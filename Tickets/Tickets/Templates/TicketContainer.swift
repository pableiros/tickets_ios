//
//  TicketContainer.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class TicketContainer: ListBaseContainer, NSCopying {
    
    var fechaInicio: String?
    var fechaFin: String?
    var folio: String?
    var descripcion: String?
    var estatus: String? = "1"
    var moduloId: String?
    var moduloNombre: String?
    
    var syncId: String?
    
    var moduloContainer: ModuloContainer? {
        get {
            var moduloContainer: ModuloContainer?
            
            if self.moduloId != nil && self.moduloNombre != nil {
                moduloContainer = ModuloContainer()
                moduloContainer?.id = self.moduloId
                moduloContainer?.nombre = self.moduloNombre
            }
            
            return moduloContainer
        }
        set {
            self.moduloId = newValue?.id
            self.moduloNombre = newValue?.nombre
        }
    }
    
    var estatusNombre: String {
        get {
            var estatusNombre: String = ""
            
            if estatus == "1" {
                estatusNombre = Localizables.ticketDetalleTableViewControlerEnProceso.translate()
            }
            
            return estatusNombre
        }
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return TicketContainer()
    }
    
    func toTicketRealm() -> TicketRealm {
        let ticketRealm: TicketRealm = TicketRealm()
        ticketRealm.id = self.id ?? ""
        ticketRealm.fechaInicio = self.fechaInicio ?? ""
        ticketRealm.fechaFin = self.fechaFin ?? ""
        ticketRealm.descripcion = self.descripcion ?? ""
        ticketRealm.estatus = self.estatus ?? ""
        ticketRealm.folio = self.folio ?? ""
        ticketRealm.moduloId = self.moduloId ?? ""
        ticketRealm.moduloNombre = self.moduloNombre ?? ""
        ticketRealm.syncId = self.syncId ?? ""
        return ticketRealm
    }
}
