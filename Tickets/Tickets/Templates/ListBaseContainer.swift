//
//  ListBaseContainer.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class ListBaseContainer: NSObject {
    
    var id: String!
    
    func getCellValue() -> String {
        return ""
    }
    
    static func ==(lhs: ListBaseContainer, rhs: ListBaseContainer) -> Bool {
        return lhs.id == rhs.id
    }
}
