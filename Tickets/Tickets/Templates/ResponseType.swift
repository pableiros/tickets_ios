//
//  ResponseType.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

enum ResponseType {
    case success
    case error
    case nointernet
    case loading
    case initialLoading
}
