//
//  ModuloRealm.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import RealmSwift

class ModuloRealm: Object {
    
    @objc dynamic var id: String?
    @objc dynamic var nombre: String?

    func toModuloContainer() -> ModuloContainer {
        let moduloContainer: ModuloContainer = ModuloContainer()
        moduloContainer.id = self.id
        moduloContainer.nombre = self.nombre
        return moduloContainer
    }
}
