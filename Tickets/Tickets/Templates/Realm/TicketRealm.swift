//
//  TicketRealm.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import RealmSwift

class TicketRealm: Object {
    
    @objc dynamic var id: String = ""
    @objc dynamic var fechaInicio: String = ""
    @objc dynamic var fechaFin: String = ""
    @objc dynamic var folio: String = ""
    @objc dynamic var descripcion: String = ""
    @objc dynamic var estatus: String = ""
    @objc dynamic var moduloId: String = ""
    @objc dynamic var moduloNombre: String = ""

    @objc dynamic var syncId: String = ""
    
    func toTicketContainer() -> TicketContainer {
        let ticketContainer: TicketContainer = TicketContainer()
        ticketContainer.id = self.id
        ticketContainer.fechaInicio = self.fechaInicio
        ticketContainer.fechaFin = self.fechaFin
        ticketContainer.folio = self.folio
        ticketContainer.descripcion = self.descripcion
        ticketContainer.estatus = self.estatus
        ticketContainer.moduloId = self.moduloId
        ticketContainer.moduloNombre = self.moduloNombre
        ticketContainer.syncId = self.syncId
        return ticketContainer
    }
}
