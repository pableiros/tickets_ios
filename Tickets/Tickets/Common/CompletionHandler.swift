//
//  CompletionHandler.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ success: Bool, _ message: String?, _ data: AnyObject?) -> Void
