//
//  NotificationCenterExtensions.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

extension NotificationCenter {
    
    static func setObserver(_ observer: AnyObject, selector: Selector, name: String, object: AnyObject?) {
        let notificationName = Notification.Name(rawValue: name)
        NotificationCenter.default.removeObserver(observer, name: notificationName, object: object)
        NotificationCenter.default.addObserver(observer, selector: selector, name: notificationName, object: object)
    }
    
    static func setObserver(_ observer: AnyObject, selector: Selector, notificationName:  NSNotification.Name, object: AnyObject?) {
        NotificationCenter.default.removeObserver(observer, name: notificationName, object: object)
        NotificationCenter.default.addObserver(observer, selector: selector, name: notificationName, object: object)
    }
}
