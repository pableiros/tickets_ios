//
//  UIViewControllerExtensions.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

extension UIViewController {
    
    private struct AssociatedKeys {
        static var loadingAlertController: String = "loadingAlertController"
        static var appStatusBarStyle: String = "appStatusBarStyle"
    }
    
    var loadingAlertController: UIAlertController! {
        get {
            guard let alertController: UIAlertController = objc_getAssociatedObject(self, &AssociatedKeys.loadingAlertController) as? UIAlertController else {
                return UIAlertController()
            }
            
            return alertController
        }
        set(value) {
            objc_setAssociatedObject(self, &AssociatedKeys.loadingAlertController, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func showAlert(withTitle title: String, andMessage message: String) {
        self.showAlertWithPresentCompletion(withTitle: title, andMessage: message, completion: nil)
    }
    
    func showAlertWithPresentCompletion(withTitle title: String, andMessage message: String, completion: (() -> Void)?) {
        OperationQueue.main.addOperation { [unowned self] in
            let alertController: UIAlertController = AlertControllerHelper.withTitle(title, message: message)
            self.present(alertController, animated: true, completion: completion)
        }
    }
    
    func presentLoadingAlertController(withMessage message: String, completion: @escaping () -> Void) {
        self.loadingAlertController = nil
        let loadingAlertControllerHelper: LoadingAlertControllerHelper = LoadingAlertControllerHelper()
        self.loadingAlertController = loadingAlertControllerHelper.loadingController(withMessage: message)
        
        OperationQueue.main.addOperation { [unowned self] in
            self.present(self.loadingAlertController, animated: true, completion: completion)
        }
    }
    
    @objc func dismissLoadingAlertController(completion: (() -> Void)? = nil) {
        OperationQueue.main.addOperation { [unowned self] in
            self.loadingAlertController?.dismiss(animated: true, completion: completion)
        }
    }
}
