//
//  URLExtension.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

extension URL {
    func appendQuery(parameters: [String: String]?) -> URL {
        guard let parameters = parameters, let urlComponents = NSURLComponents(url: self, resolvingAgainstBaseURL: true) else {
            return self
        }
        
        var mutableQueryItems: [NSURLQueryItem] = urlComponents.queryItems as [NSURLQueryItem]? ?? []
        mutableQueryItems.append(contentsOf: parameters.map{ NSURLQueryItem(name: $0, value: $1) })
        urlComponents.queryItems = mutableQueryItems as [URLQueryItem]?
        return urlComponents.url!
    }
}
