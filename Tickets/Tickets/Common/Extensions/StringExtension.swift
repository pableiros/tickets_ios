//
//  StringExtension.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

extension String {
    
    func translate() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    func dateToHumanReadable() -> String {
        var humanReadableDate: String = self
        
        if let date: Date = Date.instance(fromyyyyMMdd: self) {
            humanReadableDate = date.formatRelativeString()
        }
        
        return humanReadableDate
    }
    
    func isFullEmpty() -> Bool {
        let trimmedString: String = self.trimmingCharacters(in: CharacterSet.whitespaces)
        return trimmedString.isEmpty
    }
}
