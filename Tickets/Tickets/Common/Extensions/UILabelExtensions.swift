//
//  UILabelExtensions.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

extension UILabel {
    func setDynamicSystemFont(size: CGFloat) {
        let systemFont: UIFont = UIFont.systemFont(ofSize: size)
        self.setDynamicFont(font: systemFont)
    }
    
    func setBoldSystemFont(size: CGFloat) {
        let boldFont: UIFont = UIFont.boldSystemFont(ofSize: size)
        self.setDynamicFont(font: boldFont)
    }
    
    func setItalicSystemFont(size: CGFloat) {
        let italicFont: UIFont = UIFont.italicSystemFont(ofSize: size)
        self.setDynamicFont(font: italicFont)
    }
    
    private func setDynamicFont(font: UIFont) {
        if #available(iOS 11.0, *) {
            self.font = UIFontMetrics.default.scaledFont(for: font)
        } else {
            let scaler: CGFloat = UIFont.preferredFont(forTextStyle: .body).pointSize / 17.0
            self.font = font
            self.font.withSize(scaler * self.font.pointSize)
            
            if #available(iOS 10.0, *) {
                self.adjustsFontForContentSizeCategory = true
            }
        }
    }
}

