//
//  DictionaryExtension.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

extension Dictionary {
    
    mutating func update(_ other: Dictionary) {
        for (key, value) in other {
            self.updateValue(value, forKey:key)
        }
    }
    
    func toJSONText() -> String {
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: self,
            options: []) {
            let theJSONText = String(data: theJSONData, encoding: .ascii)
            return theJSONText!
        } else {
            return ""
        }
    }
}

extension Dictionary where Key == String {
    func getIntValueConvertedToStringValue(forKey key: String) -> String {
        var value: String = ""
        
        if let dictValue = self[key] as? String {
            value = dictValue
        } else if let dictValue = self[key] as? Int {
            value = String(dictValue)
        }
        
        return value
    }
}
