//
//  UIViewControllerNetworkExtension.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import NotificationBannerSwift

extension UIViewController {
    
    private struct AssociatedKeys {
        static var networkConnectionViewControllerHandler: String = "networkConnectionViewControllerHandler"
        static var handleInternetConnectionRestored: String = "handleInternetConnectionRestored"
        static var handleInternetConnectionLost: String = "handleInternetConnectionLost"
        static var internetObserver: String = "internetObserver"
        static var noInternetObserver: String = "noInternetObserver"
    }
    
    var networkConnectionViewControllerHandler: NetworkConnectionViewControllerHandler! {
        get {
            guard let networkConnectionViewControllerHandler = objc_getAssociatedObject(self, &AssociatedKeys.networkConnectionViewControllerHandler) as? NetworkConnectionViewControllerHandler else {
                self.networkConnectionViewControllerHandler = NetworkConnectionViewControllerHandler()
                return self.networkConnectionViewControllerHandler
            }
            
            return networkConnectionViewControllerHandler
        }
        set(value) {
            objc_setAssociatedObject(self, &AssociatedKeys.networkConnectionViewControllerHandler, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var handleInternetConnectionLost:  (() -> Void)? {
        get {
            guard let handleInternetConnectionLost = objc_getAssociatedObject(self, &AssociatedKeys.handleInternetConnectionLost) as? (() -> Void) else {
                return nil
            }
            
            return handleInternetConnectionLost
        }
        set(value) {
            objc_setAssociatedObject(self, &AssociatedKeys.handleInternetConnectionLost, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var handleInternetConnectionRestored:  (() -> Void)? {
        get {
            guard let handleInternetConnectionLost = objc_getAssociatedObject(self, &AssociatedKeys.handleInternetConnectionRestored) as? (() -> Void) else {
                return nil
            }
            
            return handleInternetConnectionLost
        }
        set(value) {
            objc_setAssociatedObject(self, &AssociatedKeys.handleInternetConnectionRestored, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var internetObserver: NSObjectProtocol! {
        get {
            guard let internetObserver = objc_getAssociatedObject(self, &AssociatedKeys.internetObserver) as? NSObjectProtocol else {
                return NSObject()
            }
            
            return internetObserver
        }
        set(value) {
            objc_setAssociatedObject(self, &AssociatedKeys.internetObserver, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var noInternetObserver: NSObjectProtocol! {
        get {
            guard let internetObserver = objc_getAssociatedObject(self, &AssociatedKeys.noInternetObserver) as? NSObjectProtocol else {
                return NSObject()
            }
            
            return internetObserver
        }
        set(value) {
            objc_setAssociatedObject(self, &AssociatedKeys.noInternetObserver, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func setInternetObserversFromTuple(objectProtocols: (NSObjectProtocol, NSObjectProtocol)) {
        self.noInternetObserver = objectProtocols.0
        self.internetObserver = objectProtocols.1
    }
    
    func prepareInternetHandler() {
        self.networkConnectionViewControllerHandler = nil
        self.networkConnectionViewControllerHandler = NetworkConnectionViewControllerHandler()
        self.networkConnectionViewControllerHandler.noInternetConnectionCompletionHandler = self.superHandleInternetConnectionLost()
        self.networkConnectionViewControllerHandler.internetConnectionCompletionHandler = self.superHandleInternetConnectionRestored()
        self.setInternetObserversFromTuple(objectProtocols:  self.networkConnectionViewControllerHandler.startHandlingNetworkConnectionChanges()!)
    }
    
    func superHandleInternetConnectionLost() -> (() -> Void) {
        return { [unowned self] in
            if AppConfiguration.shared.isInternetToastShowed == false {
                AppConfiguration.shared.isInternetToastShowed = true
                let banner = StatusBarNotificationBanner(title: Localizables.noInternetConnection.translate(), style: .danger)
                banner.show(queuePosition: .front, bannerPosition: .top)
            }
            
            if self.handleInternetConnectionLost != nil {
                self.handleInternetConnectionLost!()
            }
        }
    }
    
    func superHandleInternetConnectionRestored() -> (() -> Void) {
        return {
            AppConfiguration.shared.isInternetToastShowed = false
            if self.handleInternetConnectionRestored != nil {
                self.handleInternetConnectionRestored!()
            }
        }
    }
    
    func removeInternetObservers() {
        if self.noInternetObserver != nil && self.internetObserver != nil {
            NotificationHelper.removeObserverNotification(self.noInternetObserver)
            NotificationHelper.removeObserverNotification(self.internetObserver)
            self.noInternetObserver = nil
            self.internetObserver = nil
        }
    }
}

