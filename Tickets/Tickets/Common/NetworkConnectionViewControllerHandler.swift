//
//  NetworkConnectionViewControllerHandler.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class NetworkConnectionViewControllerHandler {
    
    var isInternetConnection: Bool = true
    var noInternetConnectionCompletionHandler: (() -> Void)!
    var internetConnectionCompletionHandler: (() -> Void)!
    
    func startHandlingNetworkConnectionChanges(
        _ noInternetConnectionCompletionHandler: @escaping () -> Void,
        internetConnectionCompletionHandler: @escaping () -> Void) -> (noInternetObserver: NSObjectProtocol, internetObserver: NSObjectProtocol) {
        self.noInternetConnectionCompletionHandler = noInternetConnectionCompletionHandler
        self.internetConnectionCompletionHandler = internetConnectionCompletionHandler
        self.verifyInternetConnections()
        
        return self.addInternetChangesNotifications()
    }
    
    func startHandlingNetworkConnectionChanges() -> (noInternetObserver: NSObjectProtocol, internetObserver: NSObjectProtocol)? {
        guard let _ = self.noInternetConnectionCompletionHandler else { return nil }
        guard let _ = self.internetConnectionCompletionHandler else { return nil }
        self.verifyInternetConnections()
        return self.addInternetChangesNotifications()
    }
    
    fileprivate func verifyInternetConnections() {
        if NetworkHelper.isConnectedToNetwork() {
            self.configureViewForInternetConnection()
        } else {
            self.configureViewForNoInternetConnection()
        }
    }
    
    fileprivate func addInternetChangesNotifications() -> (noInternetObserver: NSObjectProtocol,
        internetObserver: NSObjectProtocol) {
            let noInternetObserver = NotificationHelper.addNotificationForNameOnMainQueue(NetworkConnectionStruct.noInternetConnectionNotificationKey) { _ in
                OperationQueue.main.addOperation {
                    self.configureViewForNoInternetConnection()
                }
            }
            
            let internetObserver = NotificationHelper.addNotificationForNameOnMainQueue(NetworkConnectionStruct.internetConnectionNotificationKey) { _ in
                OperationQueue.main.addOperation {
                    self.configureViewForInternetConnection()
                }
            }
            
            return (noInternetObserver, internetObserver)
    }
        
    fileprivate func configureViewForInternetConnection() {
        self.isInternetConnection = true
        self.internetConnectionCompletionHandler()
    }
    
    fileprivate func configureViewForNoInternetConnection() {
        self.isInternetConnection = false
        self.noInternetConnectionCompletionHandler()
    }
}

