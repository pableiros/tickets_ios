//
//  NetworkConnectionStruct.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

struct NetworkConnectionStruct {
    static let reachabilityChangedNotificationKey: String = "kReachabilityChangedNotification"
    static let noInternetConnectionNotificationKey: String = "No Internet Connection Notification Key"
    static let internetConnectionNotificationKey: String = "Internet Connection Notification Key"
}
