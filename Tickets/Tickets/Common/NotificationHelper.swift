//
//  NotificationHelper.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class NotificationHelper {
    
    static func addNotificationForNameOnMainQueue(_ notificationName: String, usingBlock: @escaping (Notification) -> Void) -> NSObjectProtocol {
        let center: NotificationCenter = NotificationCenter.default
        let queue: OperationQueue = OperationQueue.main
        return center.addObserver(forName: NSNotification.Name(rawValue: notificationName),
                                  object: nil,
                                  queue: queue,
                                  using: usingBlock)
    }
    
    static func addObserverNotificationOnMainQueue(_ observer: AnyObject, selector: Selector, name: String?, object: AnyObject?) {
        let center: NotificationCenter = NotificationCenter.default
        center.addObserver(observer, selector: selector, name: name.map { NSNotification.Name(rawValue: $0) }, object: object)
    }
    
    static func removeObserverNotification(_ observer: AnyObject) {
        let center: NotificationCenter = NotificationCenter.default
        center.removeObserver(observer)
    }
    
    static func postNotification(_ notificationName: String, object: AnyObject, userInfo: NSDictionary?) {
        let center: NotificationCenter = NotificationCenter.default
        let notification: Notification = Notification(name: Notification.Name(rawValue: notificationName),
                                                      object: object,
                                                      userInfo: userInfo as? [AnyHashable: Any])
        center.post(notification)
    }
}
