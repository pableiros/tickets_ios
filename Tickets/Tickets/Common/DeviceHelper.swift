//
//  DeviceHelper.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import DeviceKit

class DeviceHelper {
    
    var isIpad: Bool {
        get {
            return self.device.isPad
        }
    }
    
    private var device: Device!
    
    init() {
        self.device = Device()
    }
    
    func isiPhone4s() -> Bool {
        return self.device == .iPhone4s || self.device == .simulator(.iPhone4s)
    }
    
    func isiPhone5() -> Bool {
        switch self.device! {
        case .iPhone5,
             .iPhone5c,
             .iPhone5s,
             .iPhoneSE,
             .iPodTouch5,
             .iPodTouch6,
             .simulator(.iPhone5),
             .simulator(.iPhone5c),
             .simulator(.iPhone5s),
             .simulator(.iPhoneSE),
             .simulator(.iPodTouch5),
             .simulator(.iPodTouch6):
            return true
        default:
            return false
        }
    }
    
    func isiPhone6AndUp() -> Bool {
        switch self.device! {
        case .iPhone6,
             .iPhone6s,
             .iPhone6Plus,
             .iPhone6sPlus,
             .iPhone7,
             .iPhone7Plus,
             .iPhone8,
             .iPhone8Plus,
             .iPhoneX,
             .simulator(.iPhone6),
             .simulator(.iPhone6s),
             .simulator(.iPhone6Plus),
             .simulator(.iPhone6sPlus),
             .simulator(.iPhone7),
             .simulator(.iPhone7Plus),
             .simulator(.iPhone8),
             .simulator(.iPhone8Plus),
             .simulator(.iPhoneX):
            return true
        default:
            return false
        }
    }
    
    func isiPhone6to8() -> Bool {
        switch self.device! {
        case .iPhone6,
             .iPhone6s,
             .iPhone6Plus,
             .iPhone6sPlus,
             .iPhone7,
             .iPhone7Plus,
             .iPhone8,
             .iPhone8Plus,
             .simulator(.iPhone6),
             .simulator(.iPhone6s),
             .simulator(.iPhone6Plus),
             .simulator(.iPhone6sPlus),
             .simulator(.iPhone7),
             .simulator(.iPhone7Plus),
             .simulator(.iPhone8),
             .simulator(.iPhone8Plus):
            return true
        default:
            return false
        }
    }
    
    func isiPhoneX() -> Bool {
        let isIphoneX: Bool
        
        switch self.device! {
        case .iPhoneX,
             .iPhoneXs,
             .iPhoneXsMax,
             .iPhoneXr,
             .simulator(.iPhoneX),
             .simulator(.iPhoneXs),
             .simulator(.iPhoneXsMax),
             .simulator(.iPhoneXr):
            isIphoneX = true
        default:
            isIphoneX = false
        }
        
        return isIphoneX
    }
}

