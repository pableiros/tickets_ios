//
//  FormFieldFormatter.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class FormFieldFormatter {
    
    let disabledLabelTextColor: UIColor = UIColor(red: 64/255, green: 64/255, blue: 64/255, alpha: 1)
    
    func formatForm(label: UILabel, isEnabled enabled: Bool) {
        label.backgroundColor = UIColor.clear
        label.setBoldSystemFont(size: 15.0)
        
        if enabled {
            label.textColor = MainColors.formFieldTitle
        } else {
            label.textColor = self.disabledLabelTextColor
        }
    }
}

