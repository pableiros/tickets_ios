//
//  PerformCompletion.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

func performCompletion(completion: (() -> Void)?) {
    if completion != nil {
        completion!()
    }
}

func performCompletionHandler(success: Bool, message: String?, data: AnyObject?, completionHandler: ((Bool, String?, AnyObject?) -> Void)?) {
    if completionHandler != nil {
        completionHandler!(success, message, data)
    }
}
