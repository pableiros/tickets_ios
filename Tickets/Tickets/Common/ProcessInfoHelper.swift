//
//  ProcessInfoHelper.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class ProcessInfoHelper {
    
    static func getValue(fromKey key: String, withDefault defaultKey: String) -> String {
        let dict: [String: String] = ProcessInfo.processInfo.environment
        let value: String
        
        if let domain: String = dict[key] {
            value = domain
        } else {
            value = defaultKey
        }
        
        return value
    }
    
    static func isEnvironmentVariableSet(name: String) -> Bool {
        var isSet: Bool = false
        
        if let value =  ProcessInfo.processInfo.environment[name] {
            isSet = value == "1"
        }
        
        return isSet
    }
}
