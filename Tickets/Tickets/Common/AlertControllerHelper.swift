//
//  AlertControllerHelper.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class AlertControllerHelper {
    static func withTitle(_ title: String, message: String) -> UIAlertController {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction: UIAlertAction = UIAlertAction(title: Localizables.alertControllerHelperOkKey.translate(),
                                                    style: UIAlertAction.Style.default,
                                                    handler: nil)
        alertController.addAction(okAction)
        return alertController
    }
}
