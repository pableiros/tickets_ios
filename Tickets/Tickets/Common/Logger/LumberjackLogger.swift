//
//  LumberJackLogger.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import CocoaLumberjack

class LumberjackLogger: LoggerProtocol {
    
    func debug(_ message: Any) {
        let messageToLog: String = "\(message)"
        DDLogDebug(messageToLog);
    }
    
    func info(_ message: Any) {
        DDLogInfo("\(message)");
    }
    
    func error(_ message: Any) {
        DDLogError("\(message)");
    }
    
    func verbose(_ message: Any) {
        DDLogVerbose("\(message)");
    }
    
    func warning(_ message: Any) {
        DDLogWarn("\(message)");
    }
    
    func prepare() {
        DDLog.add(DDTTYLogger.sharedInstance)
    }
}
