//
//  LoggerProtocol.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

protocol LoggerProtocol {
    func debug(_ message: Any)
    func error(_ message: Any)
    func info(_ message: Any)
    func verbose(_ message: Any)
    func warning(_ message: Any)
    
    func prepare()
}
