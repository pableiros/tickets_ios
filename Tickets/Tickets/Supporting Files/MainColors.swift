//
//  MainColors.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class MainColors {
    static let formFieldTitle: UIColor = UIColor.black
    static let formFieldValue: UIColor = UIColor.black
    static let formFieldValueEmpty: UIColor = UIColor.darkGray
    
    static let invalidField: UIColor = UIColor(red: 0.988235, green: 0.4, blue: 0.4, alpha: 1)
}
