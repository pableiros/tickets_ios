//
//  Localizables.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class Localizables {
    
    static let ticketsTableViewControllerTitle: String = "ticketsTableViewControllerTitle"
    
    static let ticketFormTableViewControllerTitle: String = "ticketFormTableViewControllerTitle"
    static let ticketFormTableViewControllerModulo: String = "ticketFormTableViewControllerModulo";
    static let ticketFormTableViewControllerModuloNotSelected: String = "ticketFormTableViewControllerModuloNotSelected";
    static let ticketFormTableViewControllerDescripcion: String = "ticketFormTableViewControllerDescripcion";
    static let ticketFormTableViewControllerTicketInfo: String = "ticketFormTableViewControllerTicketInfo"
    static let ticketFormTableViewControllerNoModuloSelected: String = "ticketFormTableViewControllerNoModuloSelected";
    static let ticketFormTableViewControllerNoModuloSelectedMessage: String = "ticketFormTableViewControllerNoModuloSelectedMessage";
    static let ticketFormTableViewControllerNoDescripcion: String = "ticketFormTableViewControllerNoDescripcion";
    static let ticketFormTableViewControllerNoDescripcionMessage: String = "ticketFormTableViewControllerNoDescripcionMessage";
    static let ticketFormTableViewControllerCreando: String = "ticketFormTableViewControllerCreando"
    
    static let ticketDetalleTableViewControllerTitle: String = "ticketDetalleTableViewControllerTitle";
    static let ticketDetalleTableViewControllerFecha: String = "ticketDetalleTableViewControllerFecha";
    static let ticketDetalleTableViewControllerFolio: String = "ticketDetalleTableViewControllerFolio";
    static let ticketDetalleTableViewControllerFooter: String = "ticketDetalleTableViewControllerFooter"
    static let ticketDetalleTableViewControllerEstatus: String = "ticketDetalleTableViewControllerEstatus";
    static let ticketDetalleTableViewControlerEnProceso: String = "ticketDetalleTableViewControlerEnProceso";
    
    static let modulosTableViewControllerTitle: String = "modulosTableViewControllerTitle"
    
    static let noInternetConnection: String = "noInternetConnection"

    static let alertControllerHelperOkKey: String = "alertControllerHelperOkKey"
}
