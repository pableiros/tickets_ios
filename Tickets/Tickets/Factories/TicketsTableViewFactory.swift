//
//  TicketsTableViewFactory.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class TicketsTableViewFactory: ListDiffTableViewFactory {
    
    override func prepareFactory() {
        super.prepareFactory()
        self.isMultilineCellRequired = true
    }
    
    override func successTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: TicketTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TicketTableViewCell
        let ticketContainer: TicketContainer = self.getSelectedContainer(at: indexPath) as! TicketContainer
        
        if ticketContainer.descripcion != nil, ticketContainer.descripcion!.count > 70 {
            let descripcionPrefixed: String = String(ticketContainer.descripcion!.prefix(70))
            cell.descripcionLabel.text = descripcionPrefixed + "..."
        } else {
            cell.descripcionLabel.text = ticketContainer.descripcion
        }
        
        cell.moduloLabel.text = ticketContainer.moduloNombre
        cell.fechaLabel.text = ticketContainer.fechaInicio?.dateToHumanReadable()

        return cell
    }

    override func searchFilter(searchText: String) -> ((AnyObject) -> Bool) {
        return { ticketObj -> Bool in
            let ticketContainer: TicketContainer? = ticketObj as? TicketContainer
            var ticketFound: Bool = false
            
            if ticketContainer?.descripcion?.range(of: searchText, options: .caseInsensitive) != nil {
                ticketFound = true
            } else if ticketContainer?.moduloNombre?.range(of: searchText, options: .caseInsensitive) != nil {
                ticketFound = true
            } else if ticketContainer?.fechaInicio?.range(of: searchText, options: .caseInsensitive) != nil {
                ticketFound = true
            } else if ticketContainer?.fechaInicio?.dateToHumanReadable().range(of: searchText, options: .caseInsensitive) != nil {
                ticketFound = true
            }
            
            return ticketFound
        }
    }
}
