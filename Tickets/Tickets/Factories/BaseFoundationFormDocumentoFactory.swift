//
//  BaseFoundationFormDocumentoFactory.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class BaseFoundationFormDocumentoFactory: BaseTableViewFactory, FormValidatorFactoryDelegate {

    var registerFormDoneCell: Bool = true
    var registerFormSelectableCell: Bool = true
    var registerHorizontalSelectableCell: Bool = true
    var registerMultilineCell: Bool = false
    var registerPinLayoutCell: Bool = true
    var registerSegmentedCell: Bool = false
    
    let selectFieldCellIdentifier: String = "SelectFieldCell"
    let formHorizontalCellIdentifier: String = "FormHorizontalCell"
    let multilineCellIdentifier: String = "multilineCell"
    let segmentedCellIdentifier: String = "segmented cell"
    
    private var textFieldObservableValues: NSMutableDictionary = NSMutableDictionary()
    
    override func prepareFactory() {
        self.tableView.keyboardDismissMode = .interactive
        
        if self.registerFormSelectableCell {
            self.tableView.register(FormSelectableFieldTableViewCell.self, forCellReuseIdentifier: self.selectFieldCellIdentifier)
        }
        
        if self.registerHorizontalSelectableCell {
            self.tableView.register(UINib(nibName: "FormHorizontalTableViewCell", bundle: nil), forCellReuseIdentifier: self.formHorizontalCellIdentifier)
        }
        
        if self.registerMultilineCell {
            self.tableView.register(UINib(nibName: "FormMultilineTableViewCell", bundle:nil), forCellReuseIdentifier: self.multilineCellIdentifier)
        }
        
        if self.registerPinLayoutCell {
            self.tableView.register(FormPinLayoutTableViewCell.self, forCellReuseIdentifier: self.formCellIdentifier)
        }
        
        if self.registerMultilineCell {
            self.tableView.register(UINib(nibName: "FormMultilineTableViewCell", bundle:nil), forCellReuseIdentifier: self.multilineCellIdentifier)
        }
        
        if self.registerSegmentedCell {
            self.tableView.register(UINib(nibName: "FormSegmentedTableViewCell", bundle:nil), forCellReuseIdentifier: self.segmentedCellIdentifier)
        }
    }
    
    func formSelectableFieldTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath, formSelectableFieldContainer: FormSelectableFieldContainer) -> FormSelectableFieldTableViewCell {
        let cell: FormSelectableFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: self.selectFieldCellIdentifier, for: indexPath) as! FormSelectableFieldTableViewCell
        cell.titleLabel.text = formSelectableFieldContainer.title
        
        self.validateCell(cell: cell, isValid: formSelectableFieldContainer.isValid)
        
        if let descriptionContent: String = formSelectableFieldContainer.descriptionContent {
            cell.descriptionLabel.text = descriptionContent
            cell.descriptionLabel.font = UIFont.systemFont(ofSize: 15.0)
            if formSelectableFieldContainer.isEnabled {
                cell.descriptionLabel.textColor = MainColors.formFieldValue
            } else {
                cell.descriptionLabel.textColor = MainColors.formFieldValueEmpty
            }
        } else {
            cell.descriptionLabel.text = formSelectableFieldContainer.descriptionForNotSelected
            cell.descriptionLabel.font = UIFont.systemFont(ofSize: 15.0)
            cell.descriptionLabel.textColor = MainColors.formFieldValueEmpty
        }
        
        if formSelectableFieldContainer.isEnabled {
            cell.selectionStyle = .default
            cell.accessoryType = .disclosureIndicator
        } else {
            cell.selectionStyle = .none
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    override func successTableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        let cell = tableView.cellForRow(at: indexPath)
        let height: CGFloat
        
        if cell is FormPinLayoutTableViewCell || cell is FormSelectableFieldTableViewCell {
            height = 72
        } else {
            height = 44
        }
        
        return height
    }
    
    func restoreFormPinLayoutCell(cell: FormPinLayoutTableViewCell) {
        cell.getContentTextField().textColor = MainColors.formFieldValue
        cell.getContentTextField()?.keyboardType = .default
        cell.getContentTextField()?.inputAccessoryView = nil
        cell.getContentTextField()?.isHidden = false
        cell.getContentTextField().autocapitalizationType = UITextAutocapitalizationType.sentences
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
    }
    
    func setTextFieldObservable(cell: FormTableViewCellDataSource, formTextFieldObservable: FormTextFieldObservableContainer) {
        self.textFieldObservableValues[cell.getContentTextField().tag] = formTextFieldObservable
        
        if cell.getContentTextField()?.observationInfo != nil {
            cell.getContentTextField()?.removeObserver(self, forKeyPath: "text")
        }
        
        cell.getContentTextField()?.addObserver(self, forKeyPath: "text", options: .new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let textField: UITextField = object as? UITextField else { return }
        guard let formTextFieldObservable: FormTextFieldObservableContainer = self.textFieldObservableValues[textField.tag] as? FormTextFieldObservableContainer else { return }
        
        formTextFieldObservable.setTextToModel(textField.text!)
    }
}
