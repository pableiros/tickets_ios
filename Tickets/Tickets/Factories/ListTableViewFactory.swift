//
//  ListTableViewFactory.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class ListTableViewFactory: BaseTableViewFactory {
    
    var isMultilineCellRequired: Bool = false
    
    private var multilineCellIdentifier: String = "MultilineCell"
    
    override func prepareFactory() {
        if self.isMultilineCellRequired {
            self.tableView.register(UINib(nibName: "MultilineTableViewCell", bundle: nil), forCellReuseIdentifier: self.multilineCellIdentifier)
        }
    }
    
    override func successNumberOfSectionsInTableView() -> Int {
        return 1
    }
    
    override func successTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayContainer.count
    }
    
    override func successTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
    }
    
    func multilineTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> MultilineTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.multilineCellIdentifier, for: indexPath)
        return cell as! MultilineTableViewCell
    }
    
    override func successTableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

