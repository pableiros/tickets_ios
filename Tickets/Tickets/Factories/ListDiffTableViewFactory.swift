//
//  ListDiffTableViewController.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation
import Dwifft

class ListDiffTableViewFactory: ListTableViewFactory {
    
    var diffCalculator: TableViewDiffCalculator<String, ListBaseContainer>?
    var diffContainerArray: [ListBaseContainer] = [ListBaseContainer]() {
        didSet {
            self.updateDiffCalculator()
        }
    }
    
    var diffForSearchResultArray: [ListBaseContainer] = [ListBaseContainer]()
    var isCollectionEmpty: Bool {
        get {
            return self.diffForSearchResultArray.count == 0
        }
    }
    
    override var isEmpty: Bool {
        get {
            return self.diffForSearchResultArray.count == 0
        }
    }
    
    override required init() {
        super.init()
        self.registerBaseCells = true
    }
    
    override func prepareFactory() {
        super.prepareFactory()
        self.initDiffCalculator()
    }
    
    func initDiffCalculator() {
        self.diffCalculator = TableViewDiffCalculator(tableView: self.tableView, initialSectionedValues: SectionedValues())
        self.diffCalculator?.insertionAnimation = UITableView.RowAnimation.fade
        self.diffCalculator?.deletionAnimation = UITableView.RowAnimation.fade
    }
    
    override func successNumberOfSectionsInTableView() -> Int {
        let numberOfSections: Int
        
        if let sections: Int = self.diffCalculator?.numberOfSections() {
            numberOfSections = sections
        } else {
            numberOfSections = 0
        }
        
        return numberOfSections
    }
    
    override func successTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows: Int
        
        if let numberOfObjects: Int = self.diffCalculator?.numberOfObjects(inSection: section) {
            numberOfRows = numberOfObjects
        } else {
            numberOfRows = 0
        }
        
        return numberOfRows
    }
    
    override func successTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if self.isMultilineCellRequired {
            let cell: MultilineTableViewCell = self.multilineTableView(tableView, cellForRowAtIndexPath: indexPath)
            let listBaseContainer: ListBaseContainer = self.getSelectedContainer(at: indexPath) as! ListBaseContainer
            
            cell.titleLabel.text = listBaseContainer.getCellValue()            
            
            if let containerToCompare: ListBaseContainer = self.objectSelected as? ListBaseContainer, containerToCompare.id == listBaseContainer.id {
                cell.accessoryType = UITableViewCell.AccessoryType.checkmark
            } else {
                cell.accessoryType = UITableViewCell.AccessoryType.none
            }
            
            return cell
        } else {
            fatalError("not supported yet")
        }
    }
    
    override func successTableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        if self.isMultilineCellRequired {
            return UITableView.automaticDimension
        } else {
            fatalError("not supported yet")
        }
    }
    
    override func filterForSearchText() {
        guard self.searchText != nil else { return }
        
        if self.searchText.isEmpty {
            self.diffContainerArray = self.diffForSearchResultArray
        } else {
            let filter: (AnyObject) -> Bool = self.searchFilter(searchText: self.searchText)
            self.diffContainerArray = self.diffForSearchResultArray.filter(filter)
        }
    }
    
    private func updateDiffCalculator() {
        self.diffCalculator?.sectionedValues = SectionedValues([("", self.diffContainerArray)])
    }
    
    func getSelectedContainer(at indexPath: IndexPath) -> AnyObject {
        return (self.diffCalculator?.value(atIndexPath: indexPath))!
    }
    
    func deleteContainer(baseContainer: ListBaseContainer, index: Int) {
        self.diffForSearchResultArray.removeAll { (containerToCompare) -> Bool in
            return containerToCompare.id == baseContainer.id
        }
        
        self.diffContainerArray.remove(at: index)
        self.updateDiffCalculator()
    }
    
    func insertContainer(baseContainer: ListBaseContainer, index: Int) {
        if self.diffForSearchResultArray.count == self.diffContainerArray.count {
            self.diffForSearchResultArray.insert(baseContainer, at: index)
        } else {
            self.diffForSearchResultArray.insert(baseContainer, at: 0)
        }
        
        self.diffContainerArray.insert(baseContainer, at: index)
        self.updateDiffCalculator()
    }
}

