//
//  TicketFormFactory.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class TicketFormFactory: BaseFoundationFormDocumentoFactory {
    
    var ticketContainer: TicketContainer = TicketContainer()
    var ticketFormValidatorContainer: TicketFormValidatorContainer = TicketFormValidatorContainer()
    
    let moduloRow: Int = 0
    let descripcionRow: Int = 1
    
    func setModuloContainer(moduloContainer: ModuloContainer?) {
        self.ticketContainer.moduloContainer = moduloContainer
        self.ticketFormValidatorContainer.isModuloValid = true
        
        let indexPath: IndexPath = IndexPath(row: self.moduloRow, section: 0)
        self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
    }
    
    func updateInvalidRows() {
        var indexPaths: [IndexPath] = [IndexPath]()
        
        if self.ticketFormValidatorContainer.isModuloValid == false {
            indexPaths.append(IndexPath(row: self.moduloRow, section: 0))
        }
        
        if self.ticketFormValidatorContainer.isDescripcionValid == false {
            indexPaths.append(IndexPath(row: self.descripcionRow, section: 0))
        }
        
        self.tableView.reloadRows(at: indexPaths, with: UITableView.RowAnimation.automatic)
    }
    
    override func successNumberOfSectionsInTableView() -> Int {
        return 1
    }
    
    override func successTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func successTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        switch indexPath.row {
        case self.moduloRow:
            cell = self.moduloTableView(tableView, cellForRowAtIndexPath: indexPath)
        default:
            cell = self.ticketTableView(tableView, cellForRowAtIndexPath: indexPath)
        }
        
        return cell
    }
    
    private func moduloTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let formSelectableFieldContainer: FormSelectableFieldContainer = FormSelectableFieldContainer()
        formSelectableFieldContainer.title = Localizables.ticketFormTableViewControllerModulo.translate()
        formSelectableFieldContainer.isValid = self.ticketFormValidatorContainer.isModuloValid
        formSelectableFieldContainer.descriptionContent = self.ticketContainer.moduloNombre
        formSelectableFieldContainer.descriptionForNotSelected = Localizables.ticketFormTableViewControllerModuloNotSelected.translate()
        let cell: FormSelectableFieldTableViewCell = self.formSelectableFieldTableView(tableView,
                                                                                       cellForRowAtIndexPath: indexPath,
                                                                                       formSelectableFieldContainer: formSelectableFieldContainer)
        return cell
    }
    
    private func ticketTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: DescripcionFormTableViewCell = tableView.dequeueReusableCell(withIdentifier: "descripcionCell", for: indexPath) as! DescripcionFormTableViewCell
        
        cell.descripcionLabel.text = Localizables.ticketFormTableViewControllerDescripcion.translate()
        cell.descripcionTextView.text = self.ticketContainer.descripcion
        
        cell.textChanged = { [weak tableView, weak self] descripcion in
            self?.ticketContainer.descripcion = descripcion
            self?.validateCell(cell: cell, isValid: true)
            
            UIView.performWithoutAnimation {
                tableView?.beginUpdates()
                tableView?.endUpdates()
            }
          
            let unconvertedRect: CGRect = cell.descripcionTextView.caretRect(for: cell.descripcionTextView.selectedTextRange!.start)
            var caretRect: CGRect = cell.descripcionTextView.convert(unconvertedRect, from: tableView)
            caretRect.size.height += caretRect.size.height / 2
            let deadlineTime: DispatchTime = DispatchTime.now() + .microseconds(1)

            DispatchQueue.main.asyncAfter(deadline:deadlineTime) {
                tableView?.scrollRectToVisible(caretRect, animated: false)
            }
        }
        
        self.validateCell(cell: cell, isValid: self.ticketFormValidatorContainer.isDescripcionValid)
        
        return cell
    }
    
    override func successTableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = super.successTableView(tableView, heightForRowAtIndexPath: indexPath)
        
        if indexPath.row == self.descripcionRow {
            height = 190
        }
        
        return height
    }
}
