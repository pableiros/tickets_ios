//
//  BaseTableViewFactory.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class BaseTableViewFactory: NSObject, UIGestureRecognizerDelegate {
    
    var registerBaseCells: Bool = false
    var addSeparatorAtReponseTypeSuccessSet: Bool = true
    
    var responseType: ResponseType = ResponseType.success {
        didSet {
            self.didSetResponseType()
        }
    }
    
    weak var tableView: UITableView! {
        didSet {
            if self.registerBaseCells {
                self.tableView.register(UINib(nibName: "LoadingTableViewCell", bundle: nil), forCellReuseIdentifier: self.initialLoadingCellIdentifier)
            }
        }
    }
    
    var errorMessage: String!
    
    var searchText: String!
    var objectSelected: AnyObject!
    var arrayContainer: [AnyObject] = [AnyObject]()
    
    var arrayForSearchResult: [AnyObject] = [AnyObject]() {
        didSet {
            self.arrayContainer = self.arrayForSearchResult
        }
    }
    
    var isEmpty: Bool {
        return self.arrayForSearchResult.count == 0
    }
    
    var separatorSuccessLeftInset: CGFloat = 16
    
    let noInternetCellIdentifier: String = "No Internet Cell"
    let errorCellIdentifier: String = "ErrorCell"
    let loadingCellIdentifier: String = "LoadingCell"
    let initialLoadingCellIdentifier: String = "InitialLoadingCell"
    let emptyCellIdentifier: String = "EmptyCell"
    let formCellIdentifier: String = "FormCell"
    let noneRow: Int = -1
    private var _errorMessage: String!
    
    func prepareFactory() {
        fatalError("not implemented")
    }
    
    func textForNoInternetMessage() -> String {
        fatalError("not implemented")
    }
    
    func registerFormCell() {
        self.tableView.register(UINib(nibName: "FormTableViewCell", bundle: nil), forCellReuseIdentifier: self.formCellIdentifier)
    }
    
    func didSetResponseType() {
        switch self.responseType {
        case ResponseType.error:
            self.didSetResponseTypeToErrorNoInternet()
        case ResponseType.success,ResponseType.nointernet:
            if self.tableView != nil && self.addSeparatorAtReponseTypeSuccessSet {
                self.tableView.separatorStyle = .singleLine
                self.tableView.separatorInset = UIEdgeInsets.init(top: 0, left: self.separatorSuccessLeftInset, bottom: 0, right: 0)
            }
            
            self.didSetResponseTypeToSuccess()
        case ResponseType.loading: break
        case ResponseType.initialLoading:
            self.tableView.reloadData()
        }
    }
    
    func didSetResponseTypeToErrorNoInternet() {
        guard self.tableView != nil else { return }
        self.tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 100000, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
    }
    
    func didSetResponseTypeToSuccess() {
        guard self.tableView != nil else { return }
        guard self.tableView.tableFooterView != nil else { return }
        self.tableView.tableFooterView?.removeFromSuperview()
        self.tableView.tableFooterView = nil
    }
    
    // MARK: - table view data source
    
    func numberOfSectionsInTableView() -> Int {
        var numberOfSections: Int = 0
        
        switch self.responseType {
        case .success, .nointernet:
            numberOfSections = self.successNumberOfSectionsInTableView()
        case .error:
            numberOfSections = self.errorNumberOfSectionsInTableView()
        case .loading:
            numberOfSections = self.loadingNumberOfSectionsInTableView()
        case .initialLoading:
            numberOfSections = self.initialLoadingNumberOfSectionsInTableView()
        }
        
        return numberOfSections
    }
    
    // MARK: - TableViewCollectionFactoryProtocol
    
    func successNumberOfSectionsInTableView() -> Int {
        return self.notImplementedNumberOfSectionsInTableView()
    }
    
    func successTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notImplementedNumberOfRowsInSection(section)
    }
    
    func successTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        return self.notImplementedTableView(tableView, cellForRowAtIndexPath: indexPath)
    }
    
    func successTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.notImplementedTableView(tableView, titleForHeaderInSection: section)
    }
    
    func successTableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return self.notImplementedTableView(tableView, heightForRowAtIndexPath: indexPath)
    }
    
    func successTableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func successTableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.notImplementedTableView(tableView, heightForHeaderInSection: section)
    }
    
    func successTableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func successTableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func successTableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func successTableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func errorNumberOfSectionsInTableView() -> Int {
        return 1
    }
    
    func errorTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func errorTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: ErrorTableViewCell = tableView.dequeueReusableCell(withIdentifier: self.errorCellIdentifier, for: indexPath) as! ErrorTableViewCell
        cell.errorLabel.text = self.errorMessage
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func errorTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    func errorTableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 402
    }
    
    func errorTableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func errorTableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func errorTableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func errorTableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func errorTableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func errorTableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func loadingNumberOfSectionsInTableView() -> Int {
        return 0
    }
    
    func loadingTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func loadingTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func loadingTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    func loadingTableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 0
    }
    
    func loadingTableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func loadingTableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func loadingTableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func loadingTableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func loadingTableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func loadingTableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: - initial loading
    
    func initialLoadingNumberOfSectionsInTableView() -> Int {
        return 1
    }
    
    func initialLoadingTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func initialLoadingTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: LoadingTableViewCell = tableView.dequeueReusableCell(withIdentifier: self.initialLoadingCellIdentifier, for: indexPath) as! LoadingTableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func initialLoadingTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.notImplementedTableView(tableView, titleForHeaderInSection: section)
    }
    
    func initialLoadingTableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func initialLoadingTableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func initialLoadingTableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.notImplementedTableView(tableView, heightForHeaderInSection: section)
    }
    
    func initialLoadingTableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func initialLoadingTableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func initialLoadingTableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func initialLoadingTableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    // MARK: - table view datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRowsInSection: Int
        
        switch self.responseType {
        case ResponseType.success, ResponseType.nointernet:
            numberOfRowsInSection = self.successTableView(tableView, numberOfRowsInSection: section)
        case ResponseType.error:
            numberOfRowsInSection = self.errorTableView(tableView, numberOfRowsInSection: section)
        case ResponseType.loading:
            numberOfRowsInSection = self.loadingTableView(tableView, numberOfRowsInSection: section)
        case ResponseType.initialLoading:
            numberOfRowsInSection = self.initialLoadingTableView(tableView, numberOfRowsInSection: section)
        }
        
        return numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        switch self.responseType {
        case ResponseType.success, ResponseType.nointernet:
            cell = self.successTableView(tableView, cellForRowAtIndexPath: indexPath)
        case ResponseType.error:
            cell = self.errorTableView(tableView, cellForRowAtIndexPath: indexPath)
        case ResponseType.loading:
            cell = self.loadingTableView(tableView, cellForRowAtIndexPath: indexPath)
        case ResponseType.initialLoading:
            cell = self.initialLoadingTableView(tableView, cellForRowAtIndexPath: indexPath)
        }
        
        return cell
    }
    
    // MARK: - table view delegates
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        let height: CGFloat
        
        switch self.responseType {
        case ResponseType.success, ResponseType.nointernet:
            height = self.successTableView(tableView, heightForRowAtIndexPath: indexPath)
        case ResponseType.error:
            height = self.errorTableView(tableView, heightForRowAtIndexPath: indexPath)
        case ResponseType.loading:
            height = self.loadingTableView(tableView, heightForRowAtIndexPath: indexPath)
        case ResponseType.initialLoading:
            height = self.initialLoadingTableView(tableView, heightForRowAtIndexPath: indexPath)
        }
        
        return height
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch self.responseType {
        case ResponseType.success, ResponseType.nointernet:
            self.successTableView(tableView, willDisplay: cell, forRowAt: indexPath)
        case ResponseType.error:
            self.errorTableView(tableView, willDisplay: cell, forRowAt: indexPath)
        case ResponseType.loading:
            self.loadingTableView(tableView, willDisplay: cell, forRowAt: indexPath)
        case ResponseType.initialLoading:
            self.initialLoadingTableView(tableView, willDisplay: cell, forRowAt: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var view: UIView?
        
        switch self.responseType {
        case ResponseType.success, ResponseType.nointernet:
            view = self.successTableView(tableView, viewForHeaderInSection: section)
        case ResponseType.error:
            view = self.errorTableView(tableView, viewForHeaderInSection: section)
        case ResponseType.loading:
            view = self.loadingTableView(tableView, viewForHeaderInSection: section)
        case ResponseType.initialLoading:
            view = self.initialLoadingTableView(tableView, viewForHeaderInSection: section)
        }
        
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        var view: UIView?
        
        switch self.responseType {
        case ResponseType.success, ResponseType.nointernet:
            view = self.successTableView(tableView, viewForFooterInSection: section)
        case ResponseType.error:
            view = self.errorTableView(tableView, viewForFooterInSection: section)
        case ResponseType.loading:
            view = self.loadingTableView(tableView, viewForFooterInSection: section)
        case ResponseType.initialLoading: break
        }
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var height: CGFloat = 0
        
        switch self.responseType {
        case ResponseType.success, ResponseType.nointernet:
            height = self.successTableView(tableView, heightForHeaderInSection: section)
        case ResponseType.error:
            height = self.errorTableView(tableView, heightForHeaderInSection: section)
        case ResponseType.loading:
            height = self.loadingTableView(tableView, heightForHeaderInSection: section)
        case ResponseType.initialLoading: break
        }
        
        return height
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        var height: CGFloat = 0
        
        switch self.responseType {
        case ResponseType.success, ResponseType.nointernet:
            height = self.successTableView(tableView, heightForFooterInSection: section)
        case ResponseType.error:
            height = self.errorTableView(tableView, heightForFooterInSection: section)
        case ResponseType.loading:
            height = self.loadingTableView(tableView, heightForFooterInSection: section)
        case ResponseType.initialLoading: break
        }
        
        return height
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title: String?
        
        switch self.responseType {
        case ResponseType.success, ResponseType.nointernet:
            title = self.successTableView(tableView, titleForHeaderInSection: section)
        case ResponseType.error:
            title = self.errorTableView(tableView, titleForHeaderInSection: section)
        case ResponseType.loading:
            title = self.loadingTableView(tableView, titleForHeaderInSection: section)
        case ResponseType.initialLoading: break
        }
        
        return title
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch self.responseType {
        case ResponseType.success, ResponseType.nointernet:
            self.successTableView(tableView, didEndDisplaying: cell, forRowAt: indexPath)
        case ResponseType.error:
            self.errorTableView(tableView, didEndDisplaying: cell, forRowAt: indexPath)
        case ResponseType.loading:
            self.loadingTableView(tableView, didEndDisplaying: cell, forRowAt: indexPath)
        default: break
        }
    }
    
    // MARK: - not implementations
    
    private func notImplementedNumberOfSectionsInTableView() -> Int {
        assertionFailure("Not implemented")
        return Int()
    }
    
    private func notImplementedNumberOfRowsInSection(_ section: Int) -> Int {
        assertionFailure("Not implemented")
        return Int()
    }
    
    private func notImplementedTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        assertionFailure("Not implemented")
        return UITableViewCell()
    }
    
    private func notImplementedTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        assertionFailure("Not implemented")
        return String()
    }
    
    private func notImplementedTableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        assertionFailure("Not implemented")
        return CGFloat()
    }
    
    private func notImplementedTableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        assertionFailure("Not implemented")
        return nil
    }
    
    private func notImplementedTableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        assertionFailure("Not implemented")
        return nil
    }
    
    private func notImplementedTableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        assertionFailure("Not implemented")
        return CGFloat()
    }
    
    // MARK: - reload table view
    
    func reloadTableViewDataOnMainQueue(_ completionHandler: (() -> Void)?) {
        OperationQueue.main.addOperation { [weak self] in
            self?.tableView.reloadData()
            
            performCompletion(completion: completionHandler)
        }
    }
    
    // MARK: - search results updating
    
    final func updateSearchResults(for searchController: UISearchController) {
        if let searchText: String = searchController.searchBar.text {
            self.searchText = searchText
            self.filterForSearchText()
        } else {
            self.searchText = nil
        }
    }
    
    func searchFilter(searchText: String) -> ((AnyObject) -> Bool) {
        return { _ in return false }
    }
    
    func setArrayAndFilter(array: [AnyObject]) {
        self.arrayForSearchResult = array
        self.filterForSearchText()
    }
    
    func filterForSearchText() {
        guard self.searchText != nil else { return }
        
        if self.searchText.isEmpty {
            self.arrayContainer = self.arrayForSearchResult
        } else {
            let filter = self.searchFilter(searchText: self.searchText)
            self.arrayContainer = self.arrayForSearchResult.filter(filter)
        }
    }
    
    // MARK: - gesture recognizer delegate
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func isUserTapped(onSelectedRowFrom pickerView: UIPickerView, tapRecognizer: UITapGestureRecognizer) -> Bool {
        let rowHeight: CGFloat = pickerView.rowSize(forComponent: 0).height
        let selectedRowFrame: CGRect = pickerView.bounds.insetBy(dx: 0.0, dy: (pickerView.frame.height - rowHeight) / 2.0 )
        return (selectedRowFrame.contains(tapRecognizer.location(in: pickerView)))
    }
}

