//
//  TicketDetalleTableViewFactory.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class TicketDetalleTableViewFactory: ListTableViewFactory {
    
    var ticketContainer: TicketContainer?
    
    let fechaRow: Int = 0
    let folioRow: Int = 1
    let moduloRow: Int = 2
    let estatusRow: Int = 3
    let descripcionRow: Int = 4
    
    override func successNumberOfSectionsInTableView() -> Int {
        return 1
    }
    
    override func successTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    override func successTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        switch indexPath.row {
        case self.fechaRow:
            cell = self.fechaTableView(tableView, cellForRowAtIndexPath: indexPath)
        case self.folioRow:
            cell = self.folioTableView(tableView, cellForRowAtIndexPath: indexPath)
        case self.moduloRow:
            cell = self.moduloTableView(tableView, cellForRowAtIndexPath: indexPath)
        case self.estatusRow:
            cell = self.estatusTableView(tableView, cellForRowAtIndexPath: indexPath)
        case self.descripcionRow:
            cell = self.descripcionTableView(tableView, cellForRowAtIndexPath: indexPath)
        default:
            cell = UITableViewCell()
        }
        
        return cell
    }
    
    private func fechaTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: TitleSubtitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "horizontalCell", for: indexPath) as! TitleSubtitleTableViewCell
        cell.titleLabel.text = Localizables.ticketDetalleTableViewControllerFecha.translate()
        cell.subtitleLabel.text = self.ticketContainer?.fechaInicio?.dateToHumanReadable()
        return cell
    }
    
    private func folioTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: TitleSubtitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "horizontalCell", for: indexPath) as! TitleSubtitleTableViewCell
        cell.titleLabel.text = Localizables.ticketDetalleTableViewControllerFolio.translate()
        
        if self.ticketContainer?.folio?.isFullEmpty() == false {
            cell.subtitleLabel.text = self.ticketContainer?.folio
        } else {
            cell.subtitleLabel.text = "--"
        }
        
        return cell
    }

    private func moduloTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: TitleSubtitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "horizontalCell", for: indexPath) as! TitleSubtitleTableViewCell
        cell.titleLabel.text = Localizables.ticketFormTableViewControllerModulo.translate()
        cell.subtitleLabel.text = self.ticketContainer?.moduloNombre
        return cell
    }
    
    private func estatusTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: TitleSubtitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "horizontalCell", for: indexPath) as! TitleSubtitleTableViewCell
        cell.titleLabel.text = Localizables.ticketDetalleTableViewControllerEstatus.translate()
        cell.subtitleLabel.text = self.ticketContainer?.estatusNombre
        return cell
    }
    
    private func descripcionTableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: TitleSubtitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "verticalCell", for: indexPath) as! TitleSubtitleTableViewCell
        cell.titleLabel.text = Localizables.ticketFormTableViewControllerDescripcion.translate()
        cell.subtitleLabel.text = self.ticketContainer?.descripcion
        return cell
    }
}
