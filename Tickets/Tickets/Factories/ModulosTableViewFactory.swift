//
//  ModulosTableViewFactory.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class ModulosTableViewFactory: ListDiffTableViewFactory {
    
    required init() {
        super.init()
        self.isMultilineCellRequired = true
    }
    
    override func searchFilter(searchText: String) -> ((AnyObject) -> Bool) {
        return { moduloObj -> Bool in
            let moduloContainer: ModuloContainer? = moduloObj as? ModuloContainer
            return moduloContainer?.nombre?.range(of: searchText, options: .caseInsensitive) != nil
        }
    }
}
