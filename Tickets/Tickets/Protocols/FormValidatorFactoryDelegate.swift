//
//  FormValidatorFactoryDelegate.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

protocol FormValidatorFactoryDelegate where Self:BaseTableViewFactory {
}

extension FormValidatorFactoryDelegate {
    func validateCell(cell: FormTableViewCellDataSource, isValid: Bool, formFieldTitleColor: UIColor? = nil) {
        OperationQueue.main.addOperation {
            if isValid {
                if formFieldTitleColor != nil {
                    cell.getContentTitleLabel().textColor = formFieldTitleColor
                } else {
                    cell.getContentTitleLabel().textColor = MainColors.formFieldTitle
                }
            } else {
                cell.getContentTitleLabel().textColor = MainColors.invalidField
            }
        }
    }
}
