//
//  ListFacadeDataSource.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

@objc protocol ListFacadeDataSource {
    
    func get(completionHandler: @escaping (Bool, String?, AnyObject?) -> Void)
    func prepareGetManually()
    func requireFetchManually() -> Bool
    
    @objc optional func refreshFromCached() -> Bool
    @objc optional static func createInstance() -> ListFacadeDataSource
}
