//
//  ListModelDataSource.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

protocol ListModelDataSource {
    func get(completionHandler: @escaping (Bool, String?, AnyObject?) -> Void)
}
