//
//  FormTableViewCellDataSource.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

protocol FormTableViewCellDataSource {
    func getContentTitleLabel() -> UILabel
    func getContentDescriptionLabel() -> UILabel
    func getContentTextField() -> FormTextField!
}
