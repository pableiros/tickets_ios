//
//  FormTextField.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class FormTextField: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initTextField()
    }
    
    init() {
        super.init(frame: .zero)
        self.initTextField()
    }
    
    private func initTextField() {
        self.tintColor = UIColor.gray
        self.borderStyle = .none
        self.autocorrectionType = .no
        self.textColor = MainColors.formFieldValue
    }
}
