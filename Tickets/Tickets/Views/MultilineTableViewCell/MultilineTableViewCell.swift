//
//  MultilineTableViewCell.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

@objc class MultilineTableViewCell: UITableViewCell, FormTableViewCellDataSource {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    func getContentTitleLabel() -> UILabel {
        return self.titleLabel
    }
    
    func getContentDescriptionLabel() -> UILabel {
        fatalError("empty")
    }
    
    func getContentTextField() -> FormTextField! {
        fatalError("empty")
    }
}
