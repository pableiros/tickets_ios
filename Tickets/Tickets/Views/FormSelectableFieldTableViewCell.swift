//
//  FormSelectableFieldTableViewCell.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//


import UIKit
import PinLayout

class FormSelectableFieldTableViewCell: UITableViewCell, FormTableViewCellDataSource {
    
    let titleLabel: UILabel = UILabel()
    let descriptionLabel: UILabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        
        self.descriptionLabel.font = UIFont.systemFont(ofSize: 15.0)
        self.descriptionLabel.textColor = MainColors.formFieldValueEmpty
        self.descriptionLabel.textAlignment = NSTextAlignment.left
        let formFieldFormatter: FormFieldFormatter = FormFieldFormatter()
        formFieldFormatter.formatForm(label: self.titleLabel, isEnabled: true)
        
        self.contentView.addSubview(self.titleLabel)
        self.contentView.addSubview(self.descriptionLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.descriptionLabel.pin.left(16).bottom(8).right(8).height(20.5)
        self.titleLabel.pin.left(16).top(8).right(8).height(18)
    }
    
    // MARK: - FormTableViewCellDelegate
    
    func getContentTitleLabel() -> UILabel {
        return self.titleLabel
    }
    
    func getContentDescriptionLabel() -> UILabel {
        return self.descriptionLabel
    }
    
    func getContentTextField() -> FormTextField! {
        fatalError("no content text field")
    }
}
