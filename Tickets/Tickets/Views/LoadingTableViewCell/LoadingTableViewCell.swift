//
//  LoadingTableViewCell.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoadingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var loadingView: UIView!
    
    var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let frame = CGRect(x: 0, y: 0, width: 37, height: 37)
        let type = NVActivityIndicatorType.lineScale
        let indicator = NVActivityIndicatorView(frame: frame, type: type, color: .darkGray, padding: nil)
        self.loadingView.addSubview(indicator)
        indicator.startAnimating()
    }
}
