//
//  DescripcionFormTableViewCell.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class DescripcionFormTableViewCell: UITableViewCell, UITextViewDelegate, FormTableViewCellDataSource {

    @IBOutlet weak var descripcionTextView: UITextView!
    @IBOutlet weak var descripcionLabel: UILabel!
    
    var textChanged: ((String) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let formFieldFormatter: FormFieldFormatter = FormFieldFormatter()
        formFieldFormatter.formatForm(label: self.descripcionLabel, isEnabled: true)
        self.descripcionTextView.delegate = self
    }

    func textChanged(action: @escaping (String) -> Void) {
        self.textChanged = action
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.textChanged?(textView.text)
    }

    func getContentTitleLabel() -> UILabel {
        return self.descripcionLabel
    }
    
    func getContentDescriptionLabel() -> UILabel {
        fatalError("not implemented")
    }
    
    func getContentTextField() -> FormTextField! {
        let formTextField = FormTextField()
        formTextField.text = descripcionTextView.text
        return formTextField
    }
}
