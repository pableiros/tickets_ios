//
//  FormPinLayoutTableViewCell.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit
import PinLayout

class FormPinLayoutTableViewCell: UITableViewCell, FormTableViewCellDataSource {
    
    let contentTitleLabel: UILabel = UILabel()
    let contentTextField: FormTextField = FormTextField()
    let contentSubtitleLabel: UILabel = UILabel()
    
    let disabledTextFieldTextColor: UIColor = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = UITableViewCell.SelectionStyle.none
        
        self.contentTextField.borderStyle = UITextField.BorderStyle.none
        self.contentTextField.font = UIFont.systemFont(ofSize: 14)
        let formFieldFormatter: FormFieldFormatter = FormFieldFormatter()
        formFieldFormatter.formatForm(label: self.contentTitleLabel, isEnabled: true)
        self.contentSubtitleLabel.font = UIFont.systemFont(ofSize: 14.0)
        self.contentSubtitleLabel.isHidden = true
        self.contentView.addSubview(self.contentTitleLabel)
        self.contentView.addSubview(self.contentTextField)
        self.contentView.addSubview(self.contentSubtitleLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentTitleLabel.pin.left(16).top(8).right(8).height(17.50)
        self.contentTextField.pin.left(16).bottom(8).right(8).height(17)
        self.contentSubtitleLabel.pin.left(16).bottom(8).right(8).height(17)
    }
    
    // MARK: - FormTableViewCellProtocol
    
    func getContentTitleLabel() -> UILabel {
        return self.contentTitleLabel
    }
    
    func getContentDescriptionLabel() -> UILabel {
        return self.contentSubtitleLabel
    }
    
    func getContentTextField() -> FormTextField! {
        return self.contentTextField
    }
}
