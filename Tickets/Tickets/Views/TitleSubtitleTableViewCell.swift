//
//  TitleSubtitleTableViewCell.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class TitleSubtitleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        
        let formFieldFormatter: FormFieldFormatter = FormFieldFormatter()
        formFieldFormatter.formatForm(label: self.titleLabel, isEnabled: true)
    
        self.subtitleLabel.setDynamicSystemFont(size: 15.0)
        self.subtitleLabel.textColor = MainColors.formFieldValueEmpty
    }
}
