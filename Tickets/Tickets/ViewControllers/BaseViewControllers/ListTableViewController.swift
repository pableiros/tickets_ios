//
//  ListTableViewController.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

import UIKit

class ListTableViewController: BaseTableViewController {
    
    var factory: BaseTableViewFactory!
    var reloadTableViewFromFactory: Bool = true
    var listFacade: ListFacadeDataSource!
    var unwindSegueIdentifier: String!
    var defaultStatusBar: UIStatusBarStyle!
    var preventReloadAutomatically: Bool = false
    var addPullToRefresh: Bool = true
    var lastResponseType: ResponseType = ResponseType.loading
    var tipoLlenadoAtFirst: ResponseType = ResponseType.initialLoading
    var isStaticList: Bool = false
    var refreshTableViewWithoutAutomaticPullToRefresh: Bool = false
    var addSearchController: Bool = true
    var reloadTableViewAtViewWillAppear: Bool = false
    
    var reloadListNotificationName: String!
    var reloadListNotificationNameForUnwindSegueSimulation: String!
    var isModalList: Bool = false
    var titleToForceToAdd: String?
    var objectSelected: AnyObject!
    
    var nameForNewDataFromBackgroundFetch: String? {
        return nil
    }
    
    private var registerErrorCell: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareFactory()
        self.prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.refreshTableViewWithoutAutomaticPullToRefresh {
            self.refreshList()
        }
        
        if self.reloadTableViewAtViewWillAppear {
            self.performReloadTable()
        }
        
        if let title: String = self.titleToForceToAdd {
            self.title = title
        }
    }
    
    @objc func performReloadTable() {
        self.reloadTableViewAtViewWillAppear = false
        self.factory.arrayContainer.removeAll()
        self.factory.arrayForSearchResult.removeAll()
        self.factory.responseType = ResponseType.initialLoading
        self.performRefreshListManually()
    }
    
    func prepareForStaticList() {
        self.tipoLlenadoAtFirst = .success
        self.addPullToRefresh = false
        self.preventReloadAutomatically = true
        self.isStaticList = true
        self.registerErrorCell = false
        self.addSearchController = false
    }
    
    func openKeyboard(tableView: UITableView, at indexPath: IndexPath) {
        guard let cell: FormTableViewCellDataSource = tableView.cellForRow(at: indexPath) as? FormTableViewCellDataSource else {
            return
        }
        
        cell.getContentTextField()?.becomeFirstResponder()
    }
    
    // MARK: - to override
    
    func handleSuccessRefreshList(data: AnyObject) {
        self.factory.responseType = ResponseType.success
        
        if let array: [AnyObject] = data as? [AnyObject] {
            self.factory.arrayForSearchResult = array
        } else {
            fatalError("not type known")
        }
    }
    
    func prepareTableView() {
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.tableFooterView = UIView()
    }
    
    func completionHandlerForRefresh(completionHandler: ((Bool, String?, AnyObject?) -> Void)? = nil) -> (Bool, String?, AnyObject?) -> Void {
        return { success, message, data in
            if self.addPullToRefresh {
                self.endRefreshing()
            }
            
            if success {
                self.handleSuccessRefreshList(data: data!)
            } else {
                self.handleFailRefreshList(withOnlyMessage: message!)
            }
            
            self.lastResponseType = self.factory.responseType
            
            if self.reloadTableViewFromFactory {
                self.factory.reloadTableViewDataOnMainQueue(nil)
            } else {
                self.reloadTableViewFromFactory = true
            }
            
            performCompletionHandler(success: success,
                                     message: message,
                                     data: data,
                                     completionHandler: completionHandler)
        }
    }
    
    deinit {
        super.baseDeinit()
    }
    
    // MARK: - prepare view did load
    
    private func prepareFactory() {
        if self.isStaticList == false {
            self.factory.objectSelected = self.objectSelected
            self.factory.registerBaseCells = self.registerErrorCell
        }
        
        guard self.factory != nil else {
            fatalError("Prepare factory before super viewDidLoad call")
        }
        
        self.factory.objectSelected = self.objectSelected
        self.factory.registerBaseCells = self.registerErrorCell
        self.factory.tableView = self.tableView
        self.factory.responseType = self.tipoLlenadoAtFirst
        self.factory.prepareFactory()
    }
    
    // MARK: - prepare ui
    
    private func prepareUI() {
        if self.addPullToRefresh {
            self.refreshControl?.addTarget(self, action: #selector(self.performRefreshListManually), for: .valueChanged)
        }
        
        self.prepareTableView()
        self.prepareAutomaticPullToRefresh()
        
        self.defaultStatusBar = UIApplication.shared.statusBarStyle
    }
    
    func prepareAutomaticPullToRefresh() {
        guard self.preventReloadAutomatically == false else { return }
        
        if self.isViewDidLoad == false {
            self.beginRefreshing()
            self.isViewDidLoad = true
        }
    }
    
    // MARK: - refresh content
    
    @objc func performRefreshListManually() {
        if self.isPullToRefreshManually {
            self.listFacade.prepareGetManually()
        }
        
        self.performRefreshList()
    }
    
    func performRefreshList() {
        self.refreshList()
        
        self.isPullToRefreshManually = true
    }
    
    func refreshList() {
        self.listFacade.get(completionHandler: self.completionHandlerForRefresh())
    }
    
    func handleFailRefreshList(withOnlyMessage message: String)  {
        self.factory.responseType = .error
        self.factory.errorMessage = message
        self.factory.reloadTableViewDataOnMainQueue(nil)
    }
    
    // MARK: - table view datasource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.factory.numberOfSectionsInTableView()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.factory.tableView(tableView, numberOfRowsInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.factory.tableView(tableView, cellForRowAtIndexPath: indexPath)
    }
    
    // MARK: - table view delegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.factory.tableView(tableView, heightForRowAtIndexPath: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard self.isStaticList == false else { return }
        
        if self.factory.responseType == .success {
            self.objectSelected = self.factory.arrayContainer[indexPath.row]
            if self.searchController.isActive {                
                self.searchController.isActive = false
            } else {
                self.performSegue(withIdentifier: self.unwindSegueIdentifier, sender: indexPath)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.factory.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.factory.tableView(tableView, didEndDisplaying: cell, forRowAt: indexPath)
    }
    
    // MARK: - ui search results updating
    
    override func updateSearchResults(for searchController: UISearchController) {
        guard self.factory.responseType == .success else { return }
        self.factory.updateSearchResults(for: searchController)
        self.tableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        guard self.objectSelected != nil else { return }
        Global.shared.unwindSeguePerformed = self.unwindSegueIdentifier
        Global.shared.objectListSelected = self.objectSelected
        
        let deviceHelper: DeviceHelper = DeviceHelper()
        
        if deviceHelper.isIpad && self.reloadListNotificationNameForUnwindSegueSimulation != nil {
            NotificationCenter.default.post(name: Notification.Name(rawValue: self.reloadListNotificationNameForUnwindSegueSimulation), object: nil)
        }
        
        if self.isModalList, let _ = self.presentingViewController?.presentingViewController {
            self.dismiss(animated: true, completion: nil)
        } else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
}
