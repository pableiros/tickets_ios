//
//  BaseTableViewController.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController, UISearchResultsUpdating, UISearchBarDelegate {
    
    var searchController: UISearchController!
    private var _addSearchBarOnNavigationBar: Bool!
    var addSearchBarOnNavigationBar: Bool {
        get {
            if self._addSearchBarOnNavigationBar == nil {
                if #available(iOS 11, *) {
                    self._addSearchBarOnNavigationBar = true
                } else {
                    self._addSearchBarOnNavigationBar = false
                }
            }
            
            return self._addSearchBarOnNavigationBar
        } set {
            self._addSearchBarOnNavigationBar = newValue
        }
    }
    
    var isViewDidLoad: Bool = false
    var isPullToRefreshManually: Bool = false
    
    var definesPresentationContextForSearchController: Bool = true
    var hidesNavigationBarDuringPresentation: Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.prepareInternetHandler()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.endEditing(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.removeInternetObservers()
        super.viewDidDisappear(animated)
    }

    @IBAction func dismissModal(_ sender: Any) {
        OperationQueue.main.addOperation {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    deinit {
        guard let searchController: UISearchController = self.searchController else { return }
        
        if let superView: UIView = searchController.view.superview {
            superView.removeFromSuperview()
        }
    }
    
    func baseDeinit() {
        self.deinitSearchController()
        self.deinitNotifications()
    }
    
    func deinitSearchController() {
        guard let searchController: UISearchController = self.searchController else { return }
        
        if let superView: UIView = searchController.view.superview {
            superView.removeFromSuperview()
        }
    }
    
    func deinitNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - search controller
    
    func instanceOfSearchController() -> UISearchController {
        return UISearchController(searchResultsController: nil)
    }
    
    func prepareSearchController() {
        self.searchController = self.instanceOfSearchController()
        if let searchResultsController: UISearchResultsUpdating = self.searchController.searchResultsController as? UISearchResultsUpdating {
            self.searchController.searchResultsUpdater = searchResultsController
        } else {
            self.searchController.searchResultsUpdater = self
        }
        
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.delegate = self
        self.definesPresentationContext = self.definesPresentationContextForSearchController
        self.searchController.definesPresentationContext = true
        
        if #available(iOS 11.0, *) {
            if self.addSearchBarOnNavigationBar {
                self.addSearchControllerOnNavigationController()
            } else {
                self.addSearchControllerOnTableViewHeader()
            }
        } else {
            self.addSearchControllerOnTableViewHeader()
        }
    }
    
    private func addSearchControllerOnNavigationController() {
        if #available(iOS 11.0, *) {
            self.navigationItem.searchController = self.searchController
            self.navigationItem.searchController?.hidesNavigationBarDuringPresentation = self.hidesNavigationBarDuringPresentation
        }
    }
    
    private func addSearchControllerOnTableViewHeader() {
        self.automaticallyAdjustsScrollViewInsets = true
        self.searchController.searchBar.autoresizingMask = UIView.AutoresizingMask.flexibleWidth
        self.searchController.searchBar.sizeToFit()
        self.searchController.searchBar.layoutIfNeeded()
        self.tableView.tableHeaderView = self.searchController.searchBar
    }
    
    func prepareBigNavigationTitleWithSearchController(title: String) {
        if #available(iOS 11.0, *) {
            self.addSearchBarOnNavigationBar = true
        } else {
            self.addSearchBarOnNavigationBar = false
        }
        
        if self.searchController == nil {
            self.prepareSearchController()
        }
        
        if #available(iOS 11.0, *) {
            self.addSearchBarOnNavigationBar = true
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationItem.searchController = self.searchController
            self.navigationItem.hidesSearchBarWhenScrolling = true
        }
        
        self.navigationItem.title = title
    }
    
    // MARK: - refresh control
    
    func endRefreshing() {
        if #available(iOS 11, *) {
            self.refreshControl?.endRefreshing()
        } else {
            OperationQueue.main.addOperation {
                self.refreshControl?.endRefreshing()
            }
        }
    }
    
    func beginRefreshing() {
        OperationQueue.main.addOperation {
            self.refreshControl?.sendActions(for: .valueChanged)
        }
    }
    
    @IBAction func performRefreshTableView() {
        assertionFailure("not implemented")
    }
    
    // MARK: - Search controller delegates
    
    func updateSearchResults(for searchController: UISearchController) {
        self.tableView.reloadData()
    }
}
