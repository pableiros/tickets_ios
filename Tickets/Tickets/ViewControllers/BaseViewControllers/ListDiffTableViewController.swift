//
//  ListDiffTableViewController.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class ListDiffTableViewController: ListTableViewController {
    
    var requiresSortResult: Bool = false
    var updateListUsingSearchContentAfterRefresh: Bool = false
    var isUpdateSearchResultCalledFromRefresh: Bool = false
    
    var listDiffFactory: ListDiffTableViewFactory {
        get {
            return self.factory as! ListDiffTableViewFactory
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.reloadTableViewAtViewWillAppear {
            self.listDiffFactory.diffForSearchResultArray.removeAll()
            self.listDiffFactory.diffContainerArray.removeAll()
            self.listDiffFactory.diffCalculator = nil
            self.listDiffFactory.initDiffCalculator()
        }
        
        super.viewWillAppear(animated)
    }
    
    override func performReloadTable() {
        self.reloadTableViewAtViewWillAppear = false
        self.listDiffFactory.diffContainerArray.removeAll()
        self.listDiffFactory.diffForSearchResultArray.removeAll()
        self.factory.responseType = ResponseType.initialLoading
        self.performRefreshListManually()
    }
    
    override func completionHandlerForRefresh(completionHandler: ((Bool, String?, AnyObject?) -> Void)? = nil) -> (Bool, String?, AnyObject?) -> Void {
        return { [unowned self] success, message, data in
            self.endRefreshing()
            
            if success {
                let diffArray: [ListBaseContainer]
                let listBaseContainerArray: [ListBaseContainer] = data as! NSArray as! [ListBaseContainer]
                
                if self.requiresSortResult {
                    diffArray = self.sortResult(listBaseContainerArray: listBaseContainerArray)
                } else {
                    diffArray = listBaseContainerArray
                }
                
                self.factory.responseType = .success
                
                if (self.factory as! ListDiffTableViewFactory).diffForSearchResultArray.count == 0 {
                    self.tableView.reloadData()
                }
                
                (self.factory as! ListDiffTableViewFactory).diffContainerArray = diffArray
                (self.factory as! ListDiffTableViewFactory).diffForSearchResultArray = diffArray
            } else {
                self.handleFailRefreshList(withOnlyMessage: message!)
            }
            
            self.lastResponseType = self.factory.responseType
            
            if self.updateListUsingSearchContentAfterRefresh {
                self.updateListUsingSearchContentAfterRefresh = false
                self.isUpdateSearchResultCalledFromRefresh = true
                self.updateSearchResults(for: self.searchController)
            } else {
                self.isUpdateSearchResultCalledFromRefresh = false
            }
            
            performCompletionHandler(success: success, message: message, data: data, completionHandler: completionHandler)
        }
    }
    
    func sortResult(listBaseContainerArray: [ListBaseContainer]) -> [ListBaseContainer] {
        return listBaseContainerArray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard self.isStaticList == false && self.factory.responseType == .success && self.listDiffFactory.isCollectionEmpty == false else {
            return
        }
        
        self.objectSelected = (self.factory as! ListDiffTableViewFactory).diffContainerArray[indexPath.row]
        if self.searchController.isActive {
            self.searchController.isActive = false
        } else {
            self.performSegue(withIdentifier: self.unwindSegueIdentifier, sender: indexPath)
        }
    }
}

