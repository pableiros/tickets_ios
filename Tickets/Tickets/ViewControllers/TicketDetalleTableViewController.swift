//
//  TicketDetalleTableViewController.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class TicketDetalleTableViewController: ListTableViewController {
    
    var ticketContainer: TicketContainer!
    
    override func viewDidLoad() {
        let ticketDetalleFactory: TicketDetalleTableViewFactory = TicketDetalleTableViewFactory()
        let ticketLocalData: TicketLocalData = TicketLocalData()
        self.ticketContainer = ticketLocalData.getTicket(ticketContainer: self.ticketContainer)
        ticketDetalleFactory.ticketContainer = self.ticketContainer
        
        self.factory = ticketDetalleFactory
        self.prepareForStaticList()
        super.viewDidLoad()
        
        self.navigationItem.title = Localizables.ticketDetalleTableViewControllerTitle.translate()
        
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = UINavigationItem.LargeTitleDisplayMode.never
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Localizables.ticketFormTableViewControllerTicketInfo.translate()
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        var footer: String?
        
        if self.ticketContainer.folio?.isFullEmpty() == true {
            footer = Localizables.ticketDetalleTableViewControllerFooter.translate()
        }
        
        return footer
    }
}
