//
//  TicketsTableViewController.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class TicketsTableViewController: ListDiffTableViewController {
    
    override func viewDidLoad() {
        self.factory = TicketsTableViewFactory()
        self.listFacade = TicketFacade()
        super.viewDidLoad()
        self.prepareUI()
        GlobalModulos.shared.performGetModulos()
    
        self.handleInternetConnectionRestored = {
            (self.listFacade as! TicketFacade).performBackgroundCreation()
        }
    }
    
    private func prepareUI() {
        self.prepareSearchController()
        
        self.navigationItem.title = Localizables.ticketsTableViewControllerTitle.translate()
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationItem.largeTitleDisplayMode = .always
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ticketContainer: TicketContainer? = self.listDiffFactory.getSelectedContainer(at: indexPath) as? TicketContainer
        self.performSegue(withIdentifier: "show ticket detalle", sender: ticketContainer)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let ticketContainer: TicketContainer = sender as? TicketContainer,
            let ticketDetalleTableViewController: TicketDetalleTableViewController = segue.destination as? TicketDetalleTableViewController {
            ticketDetalleTableViewController.ticketContainer = ticketContainer
        }
    }
    
    @IBAction func ticketCreated(withUnwindSegue unwindSegue: UIStoryboardSegue) {
        self.performReloadTable()
    }
}
