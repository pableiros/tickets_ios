//
//  ModulosTableViewController.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class ModulosTableViewController: ListDiffTableViewController {
    
    override func viewDidLoad() {
        self.factory = ModulosTableViewFactory()
        self.listFacade = ModuloFacade()
        self.unwindSegueIdentifier = "selectModuloWithUnwindSegue"
        super.viewDidLoad()
        self.prepareSearchController()

        self.navigationItem.title = Localizables.modulosTableViewControllerTitle.translate()
    }
}
