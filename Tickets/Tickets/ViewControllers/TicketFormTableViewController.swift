//
//  TicketFormTableViewController.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import UIKit

class TicketFormTableViewController: ListTableViewController {
    
    var ticketContainer: TicketContainer = TicketContainer()
    
    private var moduloToAdd: ModuloContainer?
    private var addModuloContainer: Bool = false
    
    private var ticketFormFactory: TicketFormFactory {
        get {
            return self.factory as! TicketFormFactory
        }
    }
    
    override func viewDidLoad() {
        let ticketFormFactory: TicketFormFactory = TicketFormFactory()
        ticketFormFactory.ticketContainer = ticketContainer
        self.factory = ticketFormFactory
        self.prepareForStaticList()
        super.viewDidLoad()
        
        self.navigationItem.title = Localizables.ticketFormTableViewControllerTitle.translate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setModuloContainer()
    }
    
    private func setModuloContainer() {
        if let moduloContainer: ModuloContainer = Global.shared.objectListSelected as? ModuloContainer {
            self.moduloToAdd = moduloContainer
            self.addModuloContainer = true
        }
        
        if self.addModuloContainer {
            self.ticketFormFactory.setModuloContainer(moduloContainer: self.moduloToAdd)
            self.moduloToAdd = nil
            self.addModuloContainer = false
        }
        
        Global.shared.objectListSelected = nil
        Global.shared.unwindSeguePerformed = nil
    }
    
    @IBAction func handleDoneButtonAction(_ sender: Any) {
        if self.isFormValid() {
            self.presentLoadingAlertController(withMessage: Localizables.ticketFormTableViewControllerCreando.translate()) {
                let ticketFacade: TicketFacade = TicketFacade()
                ticketFacade.create(ticketContainer: self.ticketFormFactory.ticketContainer, completionHandler: { (success, message, data) in
                    self.dismissLoadingAlertController(completion: {
                        OperationQueue.main.addOperation {
                            self.performSegue(withIdentifier: "ticketCreatedWithUnwindSegue:", sender: nil)
                        }
                    })
                })
            }
        }
    }
    
    private func isFormValid() -> Bool {
        let ticketFormValidator: TicketFormValidator = TicketFormValidator()
        let validatorResultContainer: ValidatorResultContainer = ticketFormValidator.validate(ticketContainer: self.ticketFormFactory.ticketContainer)
        
        if validatorResultContainer.success == false {
            self.showAlert(withTitle: validatorResultContainer.title!, andMessage: validatorResultContainer.message!)
            self.ticketFormFactory.ticketFormValidatorContainer = validatorResultContainer.validatorContainer as! TicketFormValidatorContainer
            self.ticketFormFactory.updateInvalidRows()
        }
        
        return validatorResultContainer.success
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Localizables.ticketFormTableViewControllerTicketInfo.translate()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == self.ticketFormFactory.moduloRow {
            self.performSegue(withIdentifier: "show modulos", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let modulosTableViewController: ModulosTableViewController = segue.destination as? ModulosTableViewController{
            
            modulosTableViewController.objectSelected = self.ticketFormFactory.ticketContainer.moduloContainer
        }
    }
    
    @IBAction func selectModulo(withUnwindSegue unwindSegue: UIStoryboardSegue) {
        guard let viewController: ListTableViewController = unwindSegue.source as? ListTableViewController else { return }
        self.moduloToAdd = viewController.objectSelected as? ModuloContainer
        self.addModuloContainer = true
    }
}
