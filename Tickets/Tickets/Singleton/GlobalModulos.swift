//
//  GlobalModulos.swift
//  Tickets
//
//  Created by pablo borquez on 5/29/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class GlobalModulos {
    
    static let shared: GlobalModulos = GlobalModulos()
    
    private var _operationQueue: OperationQueue!
    private var operationQueue: OperationQueue {
        get {
            if self._operationQueue == nil {
                self._operationQueue = OperationQueue()
                self._operationQueue.name = "Get modulos in background operation queue"
                self._operationQueue.maxConcurrentOperationCount = 1
            }
            
            return self._operationQueue
        }
    }
    
    func performGetModulos(completionHandler: ((Bool, String?, AnyObject?) -> Void)? = nil) {
        self.operationQueue.cancelAllOperations()
        self.operationQueue.addOperation {
            let moduloFacade: ModuloFacade = ModuloFacade()
            moduloFacade.get(completionHandler: { (success, message, data) in
                completionHandler?(success, message, data)
            })
        }
    }
}
