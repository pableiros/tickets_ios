//
//  Global.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

final class Global {
    
    static let shared: Global = Global()
    
    var unwindSeguePerformed: String!
    var objectListSelected: AnyObject!
    
    let configCache: NSCache<NSString, NSDictionary> = NSCache<NSString, NSDictionary>()
    let ticketsCache: NSCache<NSString, NSArray> = NSCache<NSString, NSArray>()
    let modulosCache: NSCache<NSString, NSArray> = NSCache<NSString, NSArray>()
}

