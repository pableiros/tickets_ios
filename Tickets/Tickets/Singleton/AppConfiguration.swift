//
//  AppConfiguration.swift
//  Tickets
//
//  Created by pablo borquez on 5/28/19.
//  Copyright © 2019 pablo borquez. All rights reserved.
//

import Foundation

class AppConfiguration {
    
    static let shared: AppConfiguration = AppConfiguration()
    
    var isAttemptingToLogout: Bool = false
    var isInternetToastShowed: Bool = false
    var showSessionRefresherWrongPasswordAtLogin: Bool = false
    
    private init() { }
    
    // MARK: - internet handlers
    
    private var _internetConnectionAvailable: Bool = true
    var isInternetConnectionAvailable: Bool {
        get {
            return self._internetConnectionAvailable
        }
        set {
            self._internetConnectionAvailable = newValue
        }
    }
    
    private var networkConnectionViewControllerHandler: NetworkConnectionViewControllerHandler!
    private var internetObserver: NSObjectProtocol!
    private var noInternetObserver: NSObjectProtocol!
    private var internetObserversHandlerTuple: (NSObjectProtocol, NSObjectProtocol) {
        set {
            self.observersHandlerTuple = newValue
            self.noInternetObserver = self.observersHandlerTuple.0
            self.internetObserver = self.observersHandlerTuple.1
        }
        
        get {
            return self.observersHandlerTuple
        }
    }
    
    private var observersHandlerTuple: (NSObjectProtocol, NSObjectProtocol)!
    
    func addInternetObserverNotifications() {
        self.removeInternetObserverNotifications()
        self.networkConnectionViewControllerHandler = nil
        self.networkConnectionViewControllerHandler = NetworkConnectionViewControllerHandler()
        self.internetObserversHandlerTuple = networkConnectionViewControllerHandler.startHandlingNetworkConnectionChanges(
            {
                self.isInternetConnectionAvailable = false
        },
            internetConnectionCompletionHandler: {
                self.isInternetConnectionAvailable = true
        })
    }
    
    func removeInternetObserverNotifications() {
        if self.noInternetObserver != nil && self.internetObserver != nil {
            NotificationHelper.removeObserverNotification(noInternetObserver)
            NotificationHelper.removeObserverNotification(internetObserver)
            self.noInternetObserver = nil
            self.internetObserver = nil
            self.observersHandlerTuple = nil
        }
    }
}

